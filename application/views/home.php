<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" id="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="title" content="NeuLetter">
    <meta name="description" content="NeuLetter">
    <meta name="keywords" content="NeuLetter">
	<link rel="shortcut icon" href="<?=site_url()?>assets/images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="<?=site_url()?>assets/images/favicon.ico" type="image/x-icon">
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-19836421-7"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-19836421-7');
	</script>
	<title>TAT</title>

	<!-- CSS -->
	<style type="text/css">
		/* .modal p{
		    display: -webkit-box;
		    -webkit-line-clamp: 5;
		    -webkit-box-orient: vertical;
		    overflow: hidden;
		    -o-text-overflow: ellipsis;
		    text-overflow: ellipsis;
		} */

		.btn:focus {
			box-shadow: unset;
		 }

		 /* .card .card-body{
			overflow-y: scroll;
			max-height: 60px;
		 } */

		 button.collapsed {
			 padding: 0;
		 }

		/* body .card-startup .card .text-green {
			 margin-bottom: 0;
		 } */

		 .footer.collapse-footer{
			 position: absolute;
			 z-index: 1000;
			 
		 }

		 .collapseButton{
			 cursor: pointer;
		 }

		 .footer.collapse-footer .d-flex {
			justify-content: center !important;
		 }

		 /* .card-startup .card-body .card-text {
			 max-height: 60px;
			 overflow-y: scroll !important;
		 } */

		 .footer.f-desktop .container-fluid .d-flex .copy-right {
			 color: #9a9a9e;
		 }

		 @media screen and (min-width: 991px) {
			 .collapse-footer {
				 display: none;
			 }

			 .footer.f-desktop .container-fluid .d-flex .copy-right{
				 /* display: none !important; */
				 margin-left: 0;
				 margin-right: auto;
			 }
		 }

		 @media screen and (max-width: 990px)  {
			.footer.f-desktop .container-fluid .d-flex a{
				 display: none !important;
			 }
		 }

		 #render_target .content2{
			 overflow-y: scroll;
		 }

		 .card-colour .card {
			 min-height: 281px;
		 }

		 .text-remark {
			color: #9a9a9e;
			font-size: 12px;
		 }
	</style>
	<link rel="stylesheet" type="text/css" href="<?=site_url()?>assets/css/style.css">
	
</head>
<body>


<div class="loader"></div>


<div class="wrapper tab-content">

	<!--/ Navbar -->
	<nav class="navbar navbar-expand-lg navbar-dark">
		<div class="container-fluid">
			<div class="row">
				<div class="col-6 col-lg-4 offset-lg-4 text-left text-lg-center">
					<a class="navbar-brand mr-auto mr-lg-0" href="<?=site_url()?>">
						<img src="<?=site_url()?>assets/images/colour-of-th-logo.png" alt="">
					</a>
				</div>
				<div class="col-6 col-lg-4 align-self-center text-right">
					<button class="navbar-toggler toggled" type="button" data-toggle="offcanvas">
						<span class="icon-bar top-bar"></span>
						<span class="icon-bar middle-bar"></span>
						<span class="icon-bar bottom-bar"></span>
					</button>
					<div class="navbar-collapse offcanvas-collapse">
						<button class="navbar-toggler toggled" type="button" data-toggle="offcanvas">
							<span class="icon-bar top-bar"></span>
							<span class="icon-bar middle-bar"></span>
							<span class="icon-bar bottom-bar"></span>
						</button>
						<div class="d-none d-lg-inline-block mr-auto">
							<ul class="nav navbar-nav">
								<li class="nav-item">
									<a class="nav-link active" data-toggle="tab" href="#section-color">Explore</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" data-toggle="tab" href="#section-startup">Travel Supporter</a>
								</li>
							</ul>
						</div>
						<div class="d-lg-none nav-mobile">
							<div class="colour-logo">
								<img src="<?=site_url()?>assets/images/colour-of-th-gray-logo.png" alt="">
							</div>
							<ul class="nav nav-tabs" role="tablist">
								<li class="nav-item">
									<a class="nav-link active" href="#tab-explore" role="tab" data-toggle="tab">Explore</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="#tab-travel-supporter" role="tab" data-toggle="tab">Travel Supporter</a>
								</li>
							</ul>
							<div class="d-block px-4 text-left">
								<div role="tabpanel" class="tab-pane fade in active" id="tab-explore">
									<ul class="nav flex-column">
										<a class="nav-link active" data-toggle="tab" href="#section-flavour">Colour of Flavour</a>
										<a class="nav-link" data-toggle="tab" href="#section-nature">Colour of Nature</a>
										<a class="nav-link" data-toggle="tab" href="#section-splash">Colour of Splash</a>
										<a class="nav-link" data-toggle="tab" href="#section-purity">Colour of Purity</a>
									</ul>
								</div>
								<div role="tabpanel" class="tab-pane fade" id="tab-travel-supporter">
									<ul class="nav flex-column">
										<a class="nav-link active" data-toggle="tab" href="#section-startup-1">Transfer and luggage services</a>
										<a class="nav-link" data-toggle="tab" href="#section-startup-2">Experiences and Lifestyles</a>
										<a class="nav-link" data-toggle="tab" href="#section-startup-3">Gastronomy</a>
										<a class="nav-link" data-toggle="tab" href="#section-startup-4">Wellness</a>
										<a class="nav-link" data-toggle="tab" href="#section-startup-5">Trip Planner</a>
										<a class="nav-link" data-toggle="tab" href="#section-startup-6">Sports</a>
									</ul>
								</div>
							</div>
						</div>
						<div class="footer collapse-footer">
							<div class="container-fluid">
								<div class="row">
									<div class="col-12 d-flex flex-row justify-content-between">
										<a class="d-inline" href="https://portal.tourismthailand.org/Privacy-Policy" target="_blank">Privacy & Policy</a>
										<a class="d-inline" href="https://portal.tourismthailand.org/Terms-of-Use" target="_blank">Terms and Conditions</a>
										<a class="d-inline" href="https://portal.tourismthailand.org/About-Thailand/About-TAT/" target="_blank">About</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</nav>

	<!--/ Color -->	
	<div class="mainContent tab-pane fade show active" id="section-color">

		<div class="headline show">
			<div class="bg-video">
				<video autoplay muted loop id="bg-video1">
					<source src="<?=site_url("media/vid/".$home->bgvid1)?>" type="video/mp4">
				</video>
			</div>
			<div class="container-fluid">
				<div class="row no-gutter">
					<div class="col-12">
						<h1>Discover <br>the Colours <br>of Thailand</h1>
						<p class="d-none d-sm-block">Experience the unforgettable journey through a great <br>variety of Thai’s local civilisations and a rich cultures.</p>
						<div class="d-table">
							<div class="d-table-cell">
								<a class="btn btn-light btn-video" role="button" data-toggle="modal" data-src="<?=$home->ytvid1?>" data-target="#videoModal">Play video</a>
							</div>
							<div class="d-table-cell pl-3 pl-md-5">
								<div class="nav-vdo">
									<a class="active">video 1</a>
									<a>video 2</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="headline">
			<div class="bg-video">
				<video autoplay muted loop id="bg-video2">
					<source src="<?=site_url("media/vid/".$home->bgvid2)?>" type="video/mp4">
				</video>
			</div>
			<div class="container-fluid">
				<div class="row no-gutter">
					<div class="col-12">
						<h1>Start <br class="d-none d-xl-block">Planning <br class="d-none d-xl-block">your journey</h1>
						<p class="d-none d-sm-block">When it comes to tourist facility, all kinds<br> of the must list has provided here to offer <br> with special deals and accommodations</p>
						<div class="d-table">
							<div class="d-table-cell">
								<a class="btn btn-light btn-video" role="button" data-toggle="modal" data-src="<?=$home->ytvid2?>" data-target="#videoModal">Play video</a>
							</div>
							<div class="d-table-cell pl-3 pl-md-5">
								<div class="nav-vdo">
									<a>video 1</a>
									<a class="active">video 2</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="explore-th">
			<div class="container-fluid">
				<div class="row no-gutter">
					<div class="col-12">
						<h6>Explore Thailand</h6>
						<div class="content card-explore-th">
							<div class="d-table">
								<div class="nav d-table-cell">
									<a class="card bg-danger" data-toggle="tab" href="#section-flavour">
										<div class="d-block d-lg-table">
											<div class="d-block d-lg-table-cell">
												<img src="<?=site_url()?>assets/images/icon-flavour.jpg" class="card-img d-none d-lg-block" alt="">
												<img src="<?=site_url()?>assets/images/icon-flavour-m.jpg" class="card-img d-block d-lg-none" alt="">
											</div>
											<div class="d-block d-lg-table-cell">
												<div class="card-body">
													<h5 class="card-title">Colour of Flavour</h5>
													<p class="card-text">Enjoy the taste <br>of thai foods</p>
												</div>
											</div>
										</div>
									</a>
								</div>
								<div class="nav d-table-cell">
									<a class="card bg-success" data-toggle="tab" href="#section-nature">
										<div class="d-block d-lg-table">
											<div class="d-block d-lg-table-cell">
												<img src="<?=site_url()?>assets/images/icon-nature.jpg" class="card-img d-none d-lg-block" alt="">
												<img src="<?=site_url()?>assets/images/icon-nature-m.jpg" class="card-img d-block d-lg-none" alt="">
											</div>
											<div class="d-block d-lg-table-cell">
												<div class="card-body">
													<h5 class="card-title">Colour of Nature</h5>
													<p class="card-text">Immerse yourself <br>in natural scene</p>
												</div>
											</div>
										</div>
									</a>
								</div>
								<div class="nav d-table-cell">
									<a class="card bg-primary" data-toggle="tab" href="#section-splash">
										<div class="d-block d-lg-table">
											<div class="d-block d-lg-table-cell">
												<img src="<?=site_url()?>assets/images/icon-splash.jpg" class="card-img d-none d-lg-block" alt="">
												<img src="<?=site_url()?>assets/images/icon-splash-m.jpg" class="card-img d-block d-lg-none" alt="">
											</div>
											<div class="d-block d-lg-table-cell">
												<div class="card-body">
													<h5 class="card-title">Colour of Splash</h5>
													<p class="card-text">Refresh life <br>with the Ocean mists</p>
												</div>
											</div>
										</div>
									</a>
								</div>
								<div class="nav d-table-cell">
									<a class="card bg-light" data-toggle="tab" href="#section-purity">
										<div class="d-block d-lg-table">
											<div class="d-block d-lg-table-cell">
												<img src="<?=site_url()?>assets/images/icon-purity.jpg" class="card-img d-none d-lg-block" alt="">
												<img src="<?=site_url()?>assets/images/icon-purity-m.jpg" class="card-img d-block d-lg-none" alt="">
											</div>
											<div class="d-block d-lg-table-cell">
												<div class="card-body">
													<h5 class="card-title">Colour of Purity</h5>
													<p class="card-text">Ease your mind <br>and body </p>
												</div>
											</div>
										</div>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>

	<!--/ Flavour -->	
	<div class="mainContent colour tab-pane fade" id="section-flavour">

		<div class="bg-border-color"></div>

		<div class="nav-color">
			<div class="container-fluid d-none d-md-block">
				<div class="row">
					<div class="col-12">
						<ul class="nav justify-content-flex-start">
							<li class="nav-item">
								<a class="nav-link active" data-toggle="tab" href="#section-flavour">Colour of Flavour</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#section-nature">Colour of Nature</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#section-splash">Colour of Splash</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#section-purity">Colour of Purity</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<div class="container-fluid">
			<div class="row">
				<div class="col-12 col-lg-3">
					<h2>The colour <br class="d-block d-sm-none d-xl-block">of Flavour</h2>
					<p class="d-none d-md-block"><small>Enjoy the taste of <br>Thai foods</small></p>
				</div>
				<div class="col-12 col-lg-9">
					<div class="content card-colour">
						<div class="d-table">
							<?
							foreach ($flavour as $key => $value) {
								?>
								<div class="d-table-cell">
									<a href="#modal-detail" onclick="render_modal('<?=$value->id?>')" data-toggle="modal" class="card">
										<img src="<?=site_url("media/explore_pic/".$value->main_pic)?>" class="card-img-top" alt="">
										<div class="card-body">
											<h5 class="card-title"><?=$value->location?></h5>
											<p class="card-text"><?=$value->what_is_it?></p>
										</div>
									</a>
								</div>
								<?
							}
							?>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>

	<!--/ Nature -->	
	<div class="mainContent colour tab-pane fade" id="section-nature">

		<div class="bg-border-color"></div>

		<div class="nav-color">
			<div class="container-fluid d-none d-md-block">
				<div class="row">
					<div class="col-12">
						<ul class="nav justify-content-flex-start">
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#section-flavour">Colour of Flavour</a>
							</li>
							<li class="nav-item">
								<a class="nav-link active" data-toggle="tab" href="#section-nature">Colour of Nature</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#section-splash">Colour of Splash</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#section-purity">Colour of Purity</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<div class="container-fluid">
			<div class="row">
				<div class="col-12 col-lg-3">
					<h2>The colour <br class="d-block d-sm-none d-xl-block">of Nature</h2>
					<p class="d-none d-md-block"><small>Immerse yourself in <br>natural scenery</small></p>
				</div>
				<div class="col-12 col-lg-9">
					<div class="content card-colour">
						<div class="d-table">
							<?
							foreach ($nature as $key => $value) {
								?>
								<div class="d-table-cell">
									<a href="#modal-detail" onclick="render_modal('<?=$value->id?>')" data-toggle="modal" class="card">
										<img src="<?=site_url("media/explore_pic/".$value->main_pic)?>" class="card-img-top" alt="">
										<div class="card-body">
											<h5 class="card-title"><?=$value->location?></h5>
											<p class="card-text"><?=$value->what_is_it?></p>
										</div>
									</a>
								</div>
								<?
							}
							?>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>

	<!--/ Splash -->	
	<div class="mainContent colour tab-pane fade" id="section-splash">

		<div class="bg-border-color"></div>

		<div class="nav-color">
			<div class="container-fluid d-none d-md-block">
				<div class="row">
					<div class="col-12">
						<ul class="nav justify-content-flex-start">
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#section-flavour">Colour of Flavour</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#section-nature">Colour of Nature</a>
							</li>
							<li class="nav-item">
								<a class="nav-link active" data-toggle="tab" href="#section-splash">Colour of Splash</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#section-purity">Colour of Purity</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<div class="container-fluid">
			<div class="row">
				<div class="col-12 col-lg-3">
					<h2>The colour <br class="d-block d-sm-none d-xl-block">of Splash</h2>
					<p class="d-none d-md-block"><small>Refresh life with <br>the Ocean mist</small></p>
				</div>
				<div class="col-12 col-lg-9">
					<div class="content card-colour">
						<div class="d-table">
							<?
							foreach ($splash as $key => $value) {
								?>
								<div class="d-table-cell">
									<a href="#modal-detail" onclick="render_modal('<?=$value->id?>')" data-toggle="modal" class="card">
										<img src="<?=site_url("media/explore_pic/".$value->main_pic)?>" class="card-img-top" alt="">
										<div class="card-body">
											<h5 class="card-title"><?=$value->location?></h5>
											<p class="card-text"><?=$value->what_is_it?></p>
										</div>
									</a>
								</div>
								<?
							}
							?>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>

	<!--/ Purity -->	
	<div class="mainContent colour tab-pane fade" id="section-purity">

		<div class="bg-border-color"></div>

		<div class="nav-color">
			<div class="container-fluid d-none d-md-block">
				<div class="row">
					<div class="col-12">
						<ul class="nav justify-content-flex-start">
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#section-flavour">Colour of Flavour</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#section-nature">Colour of Nature</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#section-splash">Colour of Splash</a>
							</li>
							<li class="nav-item">
								<a class="nav-link active" data-toggle="tab" href="#section-purity">Colour of Purity</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<div class="container-fluid">
			<div class="row">
				<div class="col-12 col-lg-3">
					<h2>The colour <br class="d-block d-sm-none d-xl-block">of Purity</h2>
					<p class="d-none d-md-block"><small>Ease your mind <br>and body</small></p>
				</div>
				<div class="col-12 col-lg-9">
					<div class="content card-colour">
						<div class="d-table">
							<?
							foreach ($purity as $key => $value) {
								?>
								<div class="d-table-cell">
									<a href="#modal-detail" onclick="render_modal('<?=$value->id?>')" data-toggle="modal" class="card">
										<img src="<?=site_url("media/explore_pic/".$value->main_pic)?>" class="card-img-top" alt="">
										<div class="card-body">
											<h5 class="card-title"><?=$value->location?></h5>
											<p class="card-text"><?=$value->what_is_it?></p>
										</div>
									</a>
								</div>
								<?
							}
							?>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>

	<!--/ Start up -->	
	<div class="mainContent colour tab-pane fade" id="section-startup">

		<div class="bg1"></div>
		<div class="bg2"></div>
		<div class="bg3"></div>
		<div class="bg4"></div>

		<div class="nav-startup">
			<div class="container-fluid d-none d-md-block">
				<div class="row">
					<div class="col-12">
						<div class="scrollnav">
							<div class="nav">
								<a class="nav-link active" data-toggle="tab" href="#section-startup-1">Transfer and luggage services</a>
								<a class="nav-link" data-toggle="tab" href="#section-startup-2">Experiences and Lifestyles</a>
								<a class="nav-link" data-toggle="tab" href="#section-startup-3">Gastronomy</a>
								<a class="nav-link" data-toggle="tab" href="#section-startup-4">Wellness</a>
								<a class="nav-link" data-toggle="tab" href="#section-startup-5">Trip Planner</a>
								<a class="nav-link" data-toggle="tab" href="#section-startup-6">Sports</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="tab-pane fade show active" id="section-startup-1">
			<div class="container-fluid">
				<div class="row">
					<div class="col-12 col-lg-3">
						<h2>Start <br class="d-none d-xl-block">Planning <br class="d-none d-xl-block">your journey</h2>
						<p class="d-none d-md-block"><small>When it comes to tourist facility, all kinds of the must list has provided here to offer with special deals and accommodations</small></p>
						<p class="text-remark">*Special deal only for Nordic traveler<br>
**For further information, please contact each supporter</p>
					</div>
					<div class="col-12 col-lg-9">
						<div class="content card-startup">
							<div class="d-table">
								<?
								foreach ($transfer as $key => $value) {
									?>
									<div class="d-table-cell">
										<div class="card">
											<div class="card-header">
												<img src="<?=site_url("media/support/".$value->main_pic)?>" class="card-img-top" alt="">
											</div>
											<div class="card-body">
												<div class="d-table">
													<div class="d-table-cell">
														<h5 class="card-title"><?=$value->name?></h5>
													</div>
													<!-- <div class="d-table-cell">
														<p><small>WELLNESS</small></p>
													</div> -->
												</div>
												<p class="card-text"><?=$value->body_text?></p>
												<div class="special-deal justify-content-between bg-white row w-100">
													<p class="text-green col-10"><b>Special Deal</b></p>
													<div class="collapseButton mx-auto col-2" >
														<img src="<?=site_url()?>assets/images/icon/keyboard_arrow_up-24px.svg" alt="icon">
													</div>
													<p class="text-grey collapse textCollapse col-12"><?=$value->deal?></p>
												</div>
											</div>
											<div class="card-footer">
												<a href="<?=$value->web_link?>" target="_blank"><p class="card-text">VISIT THIS WEBSITE</p></a>
											</div>
										</div>
									</div>
									<?
								}
								?>								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="tab-pane fade" id="section-startup-2">
			<div class="container-fluid">
				<div class="row">
					<div class="col-12 col-lg-3">
						<h2>Experiences <br class="d-none d-xl-block">and Lifestyles</h2>
						<p class="d-none d-md-block"><small>When it comes to tourist facility, all kinds of the must list has provided here to offer with special deals and accommodations</small></p>
						<p class="text-remark">*Special deal only for Nordic traveler<br>
**For further information, please contact each supporter</p>
					</div>
					<div class="col-12 col-lg-9">
						<div class="content card-startup">
							<div class="d-table">
								<?
								foreach ($lifestyles as $key => $value) {
									?>
									<div class="d-table-cell">
										<div class="card">
											<div class="card-header">
												<img src="<?=site_url("media/support/".$value->main_pic)?>" class="card-img-top" alt="">
											</div>
											<div class="card-body">
												<div class="d-table">
													<div class="d-table-cell">
														<h5 class="card-title"><?=$value->name?></h5>
													</div>
													<!-- <div class="d-table-cell">
														<p><small>WELLNESS</small></p>
													</div> -->
												</div>
												<p class="card-text"><?=$value->body_text?></p>
												<div class="special-deal justify-content-between bg-white row w-100">
													<p class="text-green col-10"><b>Special Deal</b></p>
													<div class="collapseButton mx-auto col-2" >
														<img src="<?=site_url()?>assets/images/icon/keyboard_arrow_down-24px.svg" alt="icon">
													</div>
													<p class="text-grey collapse textCollapse col-12"><?=$value->deal?></p>
												</div>
											</div>
											<div class="card-footer">
												<a href="<?=$value->web_link?>" target="_blank"><p class="card-text">VISIT THIS WEBSITE</p></a>
											</div>
										</div>
									</div>
									<?
								}
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="tab-pane fade" id="section-startup-3">
			<div class="container-fluid">
				<div class="row">
					<div class="col-12 col-lg-3">
						<h2>Gastronomy</h2>
						<p class="d-none d-md-block"><small>When it comes to tourist facility, all kinds of the must list has provided here to offer with special deals and accommodations</small></p>
						<p class="text-remark">*Special deal only for Nordic traveler<br>
**For further information, please contact each supporter</p>
					</div>
					<div class="col-12 col-lg-9">
						<div class="content card-startup">
							<div class="d-table">
								<?
								foreach ($gastronomy as $key => $value) {
									?>
									<div class="d-table-cell">
										<div class="card">
											<div class="card-header">
												<img src="<?=site_url("media/support/".$value->main_pic)?>" class="card-img-top" alt="">
											</div>
											<div class="card-body">
												<div class="d-table">
													<div class="d-table-cell">
														<h5 class="card-title"><?=$value->name?></h5>
													</div>
													<!-- <div class="d-table-cell">
														<p><small>WELLNESS</small></p>
													</div> -->
												</div>
												<p class="card-text"><?=$value->body_text?></p>
												<div class="special-deal justify-content-between bg-white row w-100">
													<p class="text-green col-10"><b>Special Deal</b></p>
													<div class="collapseButton mx-auto col-2" >
														<img src="<?=site_url()?>assets/images/icon/keyboard_arrow_down-24px.svg" alt="icon">
													</div>
													<p class="text-grey collapse textCollapse col-12"><?=$value->deal?></p>
												</div>
											</div>
											<div class="card-footer">
												<a href="<?=$value->web_link?>" target="_blank"><p class="card-text">VISIT THIS WEBSITE</p></a>
											</div>
										</div>
									</div>
									<?
								}
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="tab-pane fade" id="section-startup-4">
			<div class="container-fluid">
				<div class="row">
					<div class="col-12 col-lg-3">
						<h2>Wellness</h2>
						<p class="d-none d-md-block"><small>When it comes to tourist facility, all kinds of the must list has provided here to offer with special deals and accommodations</small></p>
						<p class="text-remark">*Special deal only for Nordic traveler<br>
**For further information, please contact each supporter</p>
					</div>
					<div class="col-12 col-lg-9">
						<div class="content card-startup">
							<div class="d-table">
								<?
								foreach ($wellness as $key => $value) {
									?>
									<div class="d-table-cell">
										<div class="card">
											<div class="card-header">
												<img src="<?=site_url("media/support/".$value->main_pic)?>" class="card-img-top" alt="">
											</div>
											<div class="card-body">
												<div class="d-table">
													<div class="d-table-cell">
														<h5 class="card-title"><?=$value->name?></h5>
													</div>
													<!-- <div class="d-table-cell">
														<p><small>WELLNESS</small></p>
													</div> -->
												</div>
												<p class="card-text"><?=$value->body_text?></p>
												<div class="special-deal justify-content-between bg-white row w-100">
													<p class="text-green col-10"><b>Special Deal</b></p>
													<div class="collapseButton mx-auto col-2" >
														<img src="<?=site_url()?>assets/images/icon/keyboard_arrow_down-24px.svg" alt="icon">
													</div>
													<p class="text-grey collapse textCollapse col-12"><?=$value->deal?></p>
												</div>
											</div>
											<div class="card-footer">
												<a href="<?=$value->web_link?>" target="_blank"><p class="card-text">VISIT THIS WEBSITE</p></a>
											</div>
										</div>
									</div>
									<?
								}
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="tab-pane fade" id="section-startup-5">
			<div class="container-fluid">
				<div class="row">
					<div class="col-12 col-lg-3">
						<h2>Trip  <br class="d-none d-xl-block">Planner</h2>
						<p class="d-none d-md-block"><small>When it comes to tourist facility, all kinds of the must list has provided here to offer with special deals and accommodations</small></p>
						<p class="text-remark">*Special deal only for Nordic traveler<br>
**For further information, please contact each supporter</p>
					</div>
					<div class="col-12 col-lg-9">
						<div class="content card-startup">
							<div class="d-table">
								<?
								foreach ($planner as $key => $value) {
									?>
									<div class="d-table-cell">
										<div class="card">
											<div class="card-header">
												<img src="<?=site_url("media/support/".$value->main_pic)?>" class="card-img-top" alt="">
											</div>
											<div class="card-body">
												<div class="d-table">
													<div class="d-table-cell">
														<h5 class="card-title"><?=$value->name?></h5>
													</div>
													<!-- <div class="d-table-cell">
														<p><small>WELLNESS</small></p>
													</div> -->
												</div>
												<p class="card-text"><?=$value->body_text?></p>
												<div class="special-deal justify-content-between bg-white row w-100">
													<p class="text-green col-10"><b>Special Deal</b></p>
													<div class="collapseButton mx-auto col-2" >
														<img src="<?=site_url()?>assets/images/icon/keyboard_arrow_down-24px.svg" alt="icon">
													</div>
													<p class="text-grey collapse textCollapse col-12"><?=$value->deal?></p>
												</div>
											</div>
											<div class="card-footer">
												<a href="<?=$value->web_link?>" target="_blank"><p class="card-text">VISIT THIS WEBSITE</p></a>
											</div>
										</div>
									</div>
									<?
								}
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="tab-pane fade" id="section-startup-6">
			<div class="container-fluid">
				<div class="row">
					<div class="col-12 col-lg-3">
						<h2>Sports</h2>
						<p class="d-none d-md-block"><small>When it comes to tourist facility, all kinds of the must list has provided here to offer with special deals and accommodations</small></p>
						<p class="text-remark">*Special deal only for Nordic traveler<br>
**For further information, please contact each supporter</p>
					</div>
					<div class="col-12 col-lg-9">
						<div class="content card-startup">
							<div class="d-table">
								<?
								foreach ($sports as $key => $value) {
									?>
									<div class="d-table-cell">
										<div class="card">
											<div class="card-header">
												<img src="<?=site_url("media/support/".$value->main_pic)?>" class="card-img-top" alt="">
											</div>
											<div class="card-body">
												<div class="d-table">
													<div class="d-table-cell">
														<h5 class="card-title"><?=$value->name?></h5>
													</div>
													<!-- <div class="d-table-cell">
														<p><small>WELLNESS</small></p>
													</div> -->
												</div>
												<p class="card-text"><?=$value->body_text?></p>
												<div class="special-deal justify-content-between bg-white row w-100">
													<p class="text-green col-10"><b>Special Deal</b></p>
													<div class="collapseButton mx-auto col-2" >
														<img src="<?=site_url()?>assets/images/icon/keyboard_arrow_down-24px.svg" alt="icon">
													</div>
													<p class="text-grey collapse textCollapse col-12"><?=$value->deal?></p>
												</div>
											</div>
											<div class="card-footer">
												<a href="<?=$value->web_link?>" target="_blank"><p class="card-text">VISIT THIS WEBSITE</p></a>
											</div>
										</div>
									</div>
									<?
								}
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>

	<!--/ Footer -->	
	<div class="footer f-desktop">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12 d-flex flex-row">
					<div class="d-inline"><img src="<?=site_url()?>assets/images/amazing-logo.png" class="logo"></div>
					<div class="d-inline"><img src="<?=site_url()?>assets/images/new-shade-logo.png" class="logo"></div>
					<div class="d-inline ml-auto copy-right" >Copyright © 2019</div> 
					<a class="d-inline ml-auto" href="https://portal.tourismthailand.org/Privacy-Policy" target="_blank">Privacy & Policy</a>
					<a class="d-inline" href="https://portal.tourismthailand.org/Terms-of-Use" target="_blank">Terms and Conditions</a>
					<a class="d-inline" href="https://portal.tourismthailand.org/About-Thailand/About-TAT/" target="_blank">About</a>
				</div>
			</div>
		</div>
	</div>

	<!--/ Notification -->	
	<div class="notification">
		<a id="close"></a>
		<h6>Take a chance to win Thai souvenir from us!</h6>
		<p>Tell us about you and your colour of Thailand.<br>
			*Remark* For the first 20 person every month, will get a little souvenir from us
		</p>
	</div>

</div>

<!-- Modal -->
<div class="modal" id="modal-detail" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="container-fluid" id="render_target">
                    <div class="row">
                        <div class="col-12 col-lg-6">
                            <div class="owl-carousel">
                                <div class="slides"><img src="<?=site_url()?>assets/images/img-475-1.jpg" alt="" /></div>
                                <div class="slides"><img src="<?=site_url()?>assets/images/img-475-2.jpg" alt="" /></div>
                                <div class="slides"><img src="<?=site_url()?>assets/images/img-475-3.jpg" alt="" /></div>
                                <div class="slides"><img src="<?=site_url()?>assets/images/img-475-4.jpg" alt="" /></div>
                            </div>
                            <div id="counter"></div>
                        </div>
                        <div class="col-12 col-lg-6">
                            <div class="content2 pl-lg-2">
                                <p class="text-small">Location</p>
                                <h5 class="modal-title">Krabi to Railay Beach Introductory Rock Climbing Experience</h5>
                                <h6>WHAT IS IT?</h6>
                                <p>We are a one-stop destination for all your beauty needs, discover and book the best beauty and wellness services around you We are a one-stop destination for all your beauty needs, discover and book the best beauty and wellness services around you…</p>
                                <h6>HOW TO GET HERE</h6>
                                <p>Accessible only by boat, but just a 15-minute ride from Ao Nang, the busiest parts of Railay are sandwiched between the scrappy, not good for swimming, beach of Hat Railay East and the high-end resorts and beautiful white sand of Hat Railay West and Hat Tham Phra Nang.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal" id="videoModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>       
				<!-- 16:9 aspect ratio -->
				<div class="embed-responsive embed-responsive-16by9">
					<iframe class="embed-responsive-item" src="" id="video"  allowscriptaccess="always" allow="autoplay" allowfullscreen></iframe>
				</div>
			</div>
		</div>
	</div>
</div> 


<!--/ Jquery-->
<script src="<?=site_url()?>assets/js/jquery.min.js"></script>
<script src="<?=site_url()?>assets/js/popper.min.js"></script>
<script src="<?=site_url()?>assets/js/bootstrap.min.js"></script>
<script src="<?=site_url()?>assets/js/jquery.easing.min.js"></script>
<script src="<?=site_url()?>assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<?=site_url()?>assets/js/owl.carousel.min.js"></script>
<script>

	// setTimeout 
    setTimeout(function(){
        $('.loader').fadeOut(1000);
    }, 200);
    setTimeout(function(){
        //$('.notification').fadeIn(1000);
    }, 5000);


	// Notification
	$(".notification #close").click(function() { 
    	$(".notification").slideUp(500);
	});


	// navbar Offcanvas
	'use strict'
	$('[data-toggle="offcanvas"]').on('click', function() {
		$('body').toggleClass('open');
		$('.offcanvas-collapse').toggleClass('open');
		$('.navbar-toggler').toggleClass('toggled');
	});

	$('.navbar-nav .nav-link, .nav.flex-column .nav-link').on('click', function() {
		$('body').toggleClass('open');
		$('.offcanvas-collapse').toggleClass('open');
		$('.navbar-toggler').toggleClass('toggled');
	});

	// Tab
	$('.nav a').on('click', function() {
		$('.nav.navbar-nav a, .nav.flex-column a, .nav > .card').removeClass('show');
		$('.nav.navbar-nav a, .nav.flex-column a, .nav > .card').removeClass('active');
	});

	$('#tab-travel-supporter .nav a').on('click', function() {
		$('.mainContent.tab-pane').removeClass('active');
		$('.mainContent.tab-pane').removeClass('show');
		$('#section-startup').addClass('active');
		$('#section-startup').addClass('show');
	});

	$('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
		var target = this.href.split('#');
		$('.nav a').filter('a[href="#'+target[1]+'"]').tab('show');
	});


	// Video
	$('.nav-vdo a').on('click', function() {
		$('.headline').toggleClass('show');
	});

	var $videoSrc;  
	$('.btn-video').click(function() {
	    $videoSrc = $(this).data( "src" );
	});
	console.log($videoSrc);

	$('#videoModal').on('shown.bs.modal', function (e) {
		$("#video").attr('src',$videoSrc + "?autoplay=1&amp;modestbranding=1&amp;showinfo=0" ); 
	})

	$('#videoModal').on('hide.bs.modal', function (e) {
	    $("#video").attr('src',$videoSrc); 
	}) 
    

	// owlCarousel
	var owl = $('.owl-carousel');
	owl.owlCarousel({
		items: 1,
		loop: false,
		nav: true,
		lazyLoad: true,
		animateIn: 'fadein',
		onInitialized: counter, 
		onTranslated: counter
	});
	function counter(event) {
		var element = event.target; 
		var items = event.item.count; 
		var item = event.item.index + 1; 
		if (item > items) {
			item = item - items
		}
		$('#counter').html(item + " / " + items)
	}


	// mCustomScrollbar
    $(".content").mCustomScrollbar({
		axis:"x",
		theme:"dark-thin",
		advanced:{autoExpandHorizontalScroll:true}
	});
	$(".content2").mCustomScrollbar({
		theme: "minimal-dark"
	});
	$(".scrollnav").mCustomScrollbar({
		axis:"x",
		theme:"minimal",
		scrollbarPosition:"outside",
	});

	function render_modal(id){
		//alert(id);
		$.ajax({
            method: "POST",
            url: "<?php echo site_url("home/render_modal"); ?>",
            data: {
                "id": id,
            }
        })
        .done(function(data) {
          $("#render_target").html(data);
          var owl = $('.owl-carousel');
			owl.owlCarousel({
				items: 1,
				loop: false,
				nav: true,
				lazyLoad: true,
				animateIn: 'fadein',
				onInitialized: counter, 
				onTranslated: counter
			});
        });
	}

	$('.collapseButton').on('click', function () {
		const isUpIcon = $(this).children('img').attr('src') === './assets/images/icon/keyboard_arrow_down-24px.svg' ? './assets/images/icon/keyboard_arrow_up-24px.svg' : './assets/images/icon/keyboard_arrow_down-24px.svg' ;
		$(this).children('img').attr('src', isUpIcon)
		$(this).siblings('.textCollapse').collapse('toggle');
	})
</script>

</body>
</html>