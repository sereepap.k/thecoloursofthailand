<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" id="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="title" content="NeuLetter">
    <meta name="description" content="NeuLetter">
    <meta name="keywords" content="NeuLetter">
	<link rel="shortcut icon" href="<?=site_url()?>assets/images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="<?=site_url()?>assets/images/favicon.ico" type="image/x-icon">
	  
	<title>TAT</title>

	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="<?=site_url()?>assets/css/style.css">

	<!--/ Jquery-->
	<script src="<?=site_url()?>assets/js/jquery.min.js"></script>
	<script src="<?=site_url()?>assets/js/popper.min.js"></script>
	<script src="<?=site_url()?>assets/js/bootstrap.min.js"></script>
	<script src="<?=site_url()?>assets/js/jquery.easing.min.js"></script>
	<script type="text/javascript">

		var count = 5; // Timer
		var redirect = "<?=site_url("home")?>"; // Target URL

		function countDown() {
	    	var timer = document.getElementById("timer"); 
	    	if (count > 0) {
				count--;
				timer.innerHTML = "We are bringing you to see <br class='d-sm-none'> all true colours of Thailand in " + count + " second…"; 
				setTimeout("countDown()", 1000);
	    	} else {
				window.location.href = redirect;
	    	}
		}
		
	</script>

</head>
<body class="bg-welcome">


<section class="welcome mx-auto d-flex align-items-center text-center">
	<div class="cover-container mx-auto">
		<img src="<?=site_url()?>assets/images/new-shade-logo.png" class="logo">
		<h1>Welcome to THAILAND!</h1>
		<p id="timer">
			<script type="text/javascript">
				countDown();
			</script>
		</p>
		<a href="<?=site_url("home")?>" class="btn btn-secondary">Let’s explore</a>
	</div>
</section>


</body>
</html>