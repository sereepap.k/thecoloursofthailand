<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>NeuMerline/VIRT : BackEnd</title>
	<link rel="stylesheet" href="<?=site_url()?>assets/admin/css.css">
	<link rel="stylesheet" href="<?=site_url()?>assets/admin/fonts/fonts.css">
</head>
<body>
<div class="sidebar">
		<div class="sidebar_search">
			<form>
				<input class="sidebar_search_input" type="text" />
				<button class="sidebar_search_button"><i class="icon-search"></i></button>
			</form>
		</div>
		<div class="sidebar_text">
			<p>Back End :<br>Web Content Management System</p>
		</div>
		<div class="sidebar_collapse scrollbar">
			<ul id="collapse">
			    <li><a class="collapse_main"><i class="icon-paragraph-right"></i>Dashbord</a>
			        <ul><li><a href="">Sub</a></li>
			            <li><a href="">Sub</a></li>
			            <li><a href="">Sub-2</a>
			                <ul><li><a href="">Sub-2</a></li>
			                    <li><a href="">Sub-2</a></li>
			                    <li><a href="">Sub-2</a>
			                        <!-- <ul><li><a href="">Sub-3</a></li></ul> -->
			                    </li>
			                </ul>
			            </li>
			        </ul>
			    </li>
			    <li><a class="collapse_main"><i class="icon-home2"></i>Home</a>
					<ul><li><a href="">Sub</a></li>
			            <li><a href="">Sub</a></li>
 						<li><a href="">Sub-2</a>
			                <ul><li><a href="">Sub-2</a></li>
			                    <li><a href="">Sub-2</a></li>
			                    <li><a href="">Sub-2</a>
			                        <!-- <ul><li><a href="">Sub-3</a></li></ul> -->
			                    </li>
			                </ul>
			            </li>
					</ul>
			    </li>
			    <li><a class="collapse_main"><i class="icon-cog"></i>Other</a>
					<ul><li><a href="">Sub</a></li>
			            <li><a href="">Sub</a></li>
 						<li><a href="">Sub-2</a>
			                <ul><li><a href="">Sub-2</a></li>
			                    <li><a href="">Sub-2</a></li>
			                    <li><a href="">Sub-2</a>
			                        <!-- <ul><li><a href="">Sub-3</a></li></ul> -->
			                    </li>
			                </ul>
			            </li>
					</ul>
			    </li>
			</ul>

		</div>
		<div class="sidebar_footer_logo">
			<img src="<?=site_url()?>assets/admin/src/logo_neu_tran.png" alt="">
			<p>COPYRIGHT 2017 Neumerlin Co.,Ltd</p>
		</div>
</div>
<div class="sidebar_mobile">
	<i class="icon-menu"></i>
</div>
<div class="dashboard">
	<div class="dashboard_nav">
		<div class="dashboard_nav_left">
			<img src="<?=site_url()?>assets/admin/src/logo_client.png" alt="">
		</div>
		<div class="dashboard_nav_right">
			<img src="http://placehold.it/50x50" id="nav_user" alt="">
			<ul id="dashboard_lang">
				<li>TH</li>
				<li style="display: none;">EN</li>
			</ul>
		</div>
		<div class="dashboard_nav_user">
			<div class="dashboard_nav_user-top">
				<h1>test</h1>
				<p>Administrator</p>
				<p class="close_nav_user">X</p>
			</div>
			<div class="dashboard_nav_user-bottom">
				<a class="dashboard_nav_user-btn" href="#">Edit Account</a>
				<a class="dashboard_nav_user-btn btn-half" href="#">Add Account</a>
				<a class="dashboard_nav_user-btn btn-half" href="#">Logout</a>
			</div>
		</div>
	</div>
	<div class="dashboard_main">
		<h1>หน้าแรก / Banner</h1>
		<button class="dash_element_mobile" id="table-option"><i class="icon-cog"></i></button>
		<div class="dash_element">
			<div class="dash_element_l">
				<button>Export</button>
				<p class="desc">*Only Excel File</p>
			</div>
			<div class="dash_element_r">
				<p>ประเภทรถยนต์</p>
				<select>
				  <option value="volvo">Honda</option>
				  <option value="toyota">Toyota</option>
				</select>
				<p>ช่วงวันที่</p>
				<input type="date" value="2017-01-01">
				<p>ถึง</p>
				<input type="date" value="2017-01-01">
				<input type="text" placeholder="ค้นหา">
				<button>ค้นหา</button>
			</div>
		</div>
		<div class="dashboard_area dashboard_area_element scrollbar scrollbar-table">
		<!-- dashboard scroll -->
			<div class="dash_table">
				<table class="scroll">
				  <thead>
				    <tr>
				      	<th>ลำดับ</th>
				      	<th>ชื่อผู้แจ้ง</th>
				      	<th>นามสกุลผู้แจ้ง</th>
				      	<th>มือถือ</th>
				      	<th>อีเมล</th>
				      	<th>ชื่อ<br>ผู้เอาประกัน</th>
				      	<th>นามสกุล<br>ผู้เอาประกัน</th>
				      	<th>หมายเลขกรมธรรม์</th>
				      	<th>รายละเอียด<br>การเกิดเหตุ</th>
				      	<th>แก้ไข/ลบ</th>
				    </tr>
				  </thead>
				  <tbody>
				    <tr>
				      	<td>1</td>
				      	<td>Lorem</td>
				      	<td>Ipsum</td>
				      	<td>0927813745</td>
				      	<td>neumerlin@gmail.com</td>
				      	<td>Lorem</td>
				      	<td>Ipsum</td>
				      	<td>12345677</td>
				      	<td>ชนท้าย</td>
				      	<td><a href="">Edit</a><br><a href="">Delete</a></td>
				    </tr>
				    <tr>
				      	<td>1</td>
				      	<td>Lorem</td>
				      	<td>Ipsum</td>
				      	<td>0927813745</td>
				      	<td>neumerlin@gmail.com</td>
				      	<td>Lorem</td>
				      	<td>Ipsum</td>
				      	<td>12345677</td>
				      	<td>ชนท้าย</td>
				      	<td><a href="">Edit</a><br><a href="">Delete</a></td>
				    </tr>
				    <tr>
				      	<td>1</td>
				      	<td>Lorem</td>
				      	<td>Ipsum</td>
				      	<td>0927813745</td>
				      	<td>neumerlin@gmail.com</td>
				      	<td>Lorem</td>
				      	<td>Ipsum</td>
				      	<td>12345677</td>
				      	<td>ชนท้าย</td>
				      	<td><a href="">Edit</a><br><a href="">Delete</a></td>
				    </tr>
				    <tr>
				      	<td>1</td>
				      	<td>Lorem</td>
				      	<td>Ipsum</td>
				      	<td>0927813745</td>
				      	<td>neumerlin@gmail.com</td>
				      	<td>Lorem</td>
				      	<td>Ipsum</td>
				      	<td>12345677</td>
				      	<td>ชนท้าย</td>
				      	<td><a href="">Edit</a><br><a href="">Delete</a></td>
				    </tr>
				    <tr>
				      	<td>1</td>
				      	<td>Lorem</td>
				      	<td>Ipsum</td>
				      	<td>0927813745</td>
				      	<td>neumerlin@gmail.com</td>
				      	<td>Lorem</td>
				      	<td>Ipsum</td>
				      	<td>12345677</td>
				      	<td>ชนท้าย</td>
				      	<td><a href="">Edit</a><br><a href="">Delete</a></td>
				    </tr>
				    <tr>
				      	<td>1</td>
				      	<td>Lorem</td>
				      	<td>Ipsum</td>
				      	<td>0927813745</td>
				      	<td>neumerlin@gmail.com</td>
				      	<td>Lorem</td>
				      	<td>Ipsum</td>
				      	<td>12345677</td>
				      	<td>ชนท้าย</td>
				      	<td><a href="">Edit</a><br><a href="">Delete</a></td>
				    </tr>
				    <tr>
				      	<td>1</td>
				      	<td>Lorem</td>
				      	<td>Ipsum</td>
				      	<td>0927813745</td>
				      	<td>neumerlin@gmail.com</td>
				      	<td>Lorem</td>
				      	<td>Ipsum</td>
				      	<td>12345677</td>
				      	<td>ชนท้าย</td>
				      	<td><a href="">Edit</a><br><a href="">Delete</a></td>
				    </tr>
				    <tr>
				      	<td>1</td>
				      	<td>Lorem</td>
				      	<td>Ipsum</td>
				      	<td>0927813745</td>
				      	<td>neumerlin@gmail.com</td>
				      	<td>Lorem</td>
				      	<td>Ipsum</td>
				      	<td>12345677</td>
				      	<td>ชนท้าย</td>
				      	<td><a href="">Edit</a><br><a href="">Delete</a></td>
				    </tr>
				    <tr>
				      	<td>1</td>
				      	<td>Lorem</td>
				      	<td>Ipsum</td>
				      	<td>0927813745</td>
				      	<td>neumerlin@gmail.com</td>
				      	<td>Lorem</td>
				      	<td>Ipsum</td>
				      	<td>12345677</td>
				      	<td>ชนท้าย</td>
				      	<td><a href="">Edit</a><br><a href="">Delete</a></td>
				    </tr>
				    <tr>
				      	<td>1</td>
				      	<td>Lorem</td>
				      	<td>Ipsum</td>
				      	<td>0927813745</td>
				      	<td>neumerlin@gmail.com</td>
				      	<td>Lorem</td>
				      	<td>Ipsum</td>
				      	<td>12345677</td>
				      	<td>ชนท้าย</td>
				      	<td><a href="">Edit</a><br><a href="">Delete</a></td>
				    </tr>
				    <tr>
				      	<td>1</td>
				      	<td>Lorem</td>
				      	<td>Ipsum</td>
				      	<td>0927813745</td>
				      	<td>neumerlin@gmail.com</td>
				      	<td>Lorem</td>
				      	<td>Ipsum</td>
				      	<td>12345677</td>
				      	<td>ชนท้าย</td>
				      	<td><a href="">Edit</a><br><a href="">Delete</a></td>
				    </tr>
				    <tr>
				      	<td>1</td>
				      	<td>Lorem</td>
				      	<td>Ipsum</td>
				      	<td>0927813745</td>
				      	<td>neumerlin@gmail.com</td>
				      	<td>Lorem</td>
				      	<td>Ipsum</td>
				      	<td>12345677</td>
				      	<td>ชนท้าย</td>
				      	<td><a href="">Edit</a><br><a href="">Delete</a></td>
				    </tr>
				  </tbody>
				</table>
			</div>
		<!-- dashboard -->
		</div>
	</div>

</div>

</body>
</html>
<script src="<?=site_url()?>assets/admin/js/jquery.min.js"></script>
<script src="<?=site_url()?>assets/admin/js/main.js"></script>

