<?
$ci =& get_instance();
?>
<div class="dashboard">	
	<?
	$this->load->view('admin/v_dashboard_nav');
	?>
	<div class="dashboard_main">
		<h1>หน้าแรก / Banner</h1>
		<button class="dash_element_mobile" id="table-option"><i class="icon-cog"></i></button>
		<div class="dash_element">
			<div class="dash_element_r">
				<p>ประเภท</p>
				<select id="type">
				  <option value="Flavor">Flavor</option>
				  <option value="Nature">Nature</option>
				  <option value="Splash">Splash</option>
				  <option value="Purity">Purity</option>
				</select>
				<button id="search_but" onclick="search()">ค้นหา</button>
			</div>
		</div>
		<div class="dashboard_area dashboard_area_element scrollbar scrollbar-table">
		<!-- dashboard scroll -->
			<div class="dash_table">
				<table class="scroll">
				  <thead>
				    <tr>
				      <th>ลำดับ</th>
				      <th>Thumbnail</th>
				      <th>Location</th>
			          <th>What is it</th>
			          <th>How to get here</th>
			          <th>Action</th>
				    </tr>
				  </thead>
				  <tbody>
				    <?
			        foreach ($explore_list as $key => $value) {
			          ?>
			          <tr>
			            <td><?=$key+1?></td>
			            <td><img src="<?php echo site_url('media/explore_pic/'.$value->main_pic); ?>" alt=""></td>
			            <td><?=$value->location?></td>
			            <td><?=$value->what_is_it?></td>            
			            <td><?=$value->how_to?></td>            
			                      
			            <td>
			            	<a href="<?=site_url("admin/explore/create/".$value->id)?>">Edit</a>
			            	<br>
			            	<a href="javascript:deleteconfirm('<?=site_url("admin/explore/delete/".$value->id)?>');">Delete</a>
			            </td>
			          </tr>
			          <?
			        }
			        ?>   
				  </tbody>
				</table>
			</div>
		<!-- dashboard -->
		</div>
	</div>
</div>
<script type="text/javascript">
      function deleteconfirm(link){
        if (confirm("Confirm Delete")) {
            window.open(link,"_self")
        };
    }
function search(){
          myform = document.createElement("form");
          $(myform).attr("action","<?=site_url("admin/explore")?>");   
          $(myform).attr("method","post");
          $(myform).html('<input type="text" name="type" value="'+$("#type").val()+'">')
          document.body.appendChild(myform);
          myform.submit();
          $(myform).remove();
        }    
</script>