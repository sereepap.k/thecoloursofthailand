<?
$ci =& get_instance();
?>
<div class="dashboard">	
	<?
	$this->load->view('admin/v_dashboard_nav');
	?>
	<div class="dashboard_main">
		<h1>หน้าแรก / Banner</h1>
		<button class="dash_element_mobile" id="table-option"><i class="icon-cog"></i></button>
		<div class="dash_element">
			<div class="dash_element_r">
				<p>ประเภท</p>
				<select id="type">
				  <option value="transfer">Transfer and luggage services</option>
				  <option value="lifestyles">Experiences and Lifestyles</option>
				  <option value="gastronomy">Gastronomy</option>
				  <option value="wellness">Wellness</option>
				  <option value="planner">Trip Planner</option>
				  <option value="sports">Sports</option>
				</select>
				<button id="search_but" onclick="search()">ค้นหา</button>
			</div>
		</div>
		<div class="dashboard_area dashboard_area_element scrollbar scrollbar-table">
		<!-- dashboard scroll -->
			<div class="dash_table">
				<table class="scroll">
				  <thead>
				    <tr>
				      <th>ลำดับ</th>
				      <th>logo</th>
			          <th>Name</th>
			          <th>Body text</th>
			          <th>Deal</th>
			          <th>Website Link</th>
			          <th>Action</th>
				    </tr>
				  </thead>
				  <tbody>
				    <?
			        foreach ($support_list as $key => $value) {
			          ?>
			          <tr>
			            <td><?=$key+1?></td>
			            <td><img src="<?php echo site_url('media/support/'.$value->main_pic); ?>" alt=""></td>
			            <td><?=$value->name?></td> 
			            <td><?=$value->body_text?></td> 
			            <td><?=$value->deal?></td>
			            <td><a href="<?=$value->web_link?>"><?=$value->web_link?></a></td>           
			            <td>
			            	<a href="<?=site_url("admin/support/create/".$value->id)?>">Edit</a>
			            	<br>
			            	<a href="javascript:deleteconfirm('<?=site_url("admin/support/delete/".$value->id)?>');">Delete</a>
			            </td>
			          </tr>
			          <?
			        }
			        ?>   
				  </tbody>
				</table>
			</div>
		<!-- dashboard -->
		</div>
	</div>
</div>
<script type="text/javascript">
      function deleteconfirm(link){
        if (confirm("Confirm Delete")) {
            window.open(link,"_self")
        };
    }
function search(){
          myform = document.createElement("form");
          $(myform).attr("action","<?=site_url("admin/support")?>");   
          $(myform).attr("method","post");
          $(myform).html('<input type="text" name="type" value="'+$("#type").val()+'">')
          document.body.appendChild(myform);
          myform.submit();
          $(myform).remove();
        }    
</script>