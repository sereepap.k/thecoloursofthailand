
	<div class="login_box">
		<div id="login-main">
				<div class="login_box_logo">
					<img src="<?=site_url()?>src/logo_client.png" alt="">
				</div>
				<div class="login_box_from">
					<form method="post" action="<?php echo site_url('admin/main/login')?>">
						<p class="login_box_catch <?php if(!isset($error_msg2)){?>none<?php }?>">Username Or Password Incorrect.</p>
						<input type="text" placeholder="Username" name="username"/>
						<input type="password" placeholder="Password" name="password"/>
			     		<button id="login-main-submit">login</button>
			     		<p><a href="#" id="login-main-btn">Forgot Your Password ?</a></p>
					</form>
				</div>
		</div>
		<div id="login-forgot">
				<div class="login_box_forgot">
					<h1>Forgot Your Password?</h1>
					<p> If you have forgotten your password,<br>please enter your account’s
						email address below and click the “Reset My Password” button.
						You will receive an email that contains a link to set a new password.
					</p>
				<form method="post" action="<?php echo site_url('admin/main/recover_pass')?>">
					<input name="email" type="text" placeholder="E-mail"/>
		     		<button>Recover My Password</button>
		     		<p><a href="#" id="login-forgot-btn"><span>&#9666;</span>Return to Login Page</a></p>
				</form>
				</div>
		</div>
	</div>
	<div class="login_footer">
		<img src="<?=site_url()?>src/logo_neu_tran.png" alt="">
		<p>Copyright 2018 Neumerlin Co.,Ltd.</p>
	</div>