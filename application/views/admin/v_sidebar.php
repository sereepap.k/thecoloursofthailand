<?
$ci =& get_instance();
?>
<div class="sidebar">
		<div class="sidebar_search">
			<form>
				<input class="sidebar_search_input" type="text" />
				<button class="sidebar_search_button"><i class="icon-search"></i></button>
			</form>
		</div>
		<div class="sidebar_text">
			<p>Back End :<br>Web Content Management System</p>
		</div>
		<div class="sidebar_collapse scrollbar">
			<ul id="collapse">
				<li><a class="collapse_main"><i class="icon-paragraph-right"></i>Admin User</a>
			        <ul><li><a href="<?=site_url("admin/Admin_user/create")?>">Add</a></li>
			            <li><a href="<?=site_url("admin/Admin_user")?>">Admin List</a></li>
			        </ul>
			    </li>
			    <li><a class="collapse_main" href="<?=site_url("admin/home")?>"><i class="icon-paragraph-right"></i>Home</a>
			    </li>
			    <li><a class="collapse_main"><i class="icon-home2"></i>Explore</a>
					<ul><li><a href="<?=site_url("admin/explore/create")?>">Add</a></li>
			            <li><a href="<?=site_url("admin/explore")?>">Explore List</a></li>
					</ul>
			    </li>
			    <li><a class="collapse_main"><i class="icon-home2"></i>Travel Supporter</a>
					<ul><li><a href="<?=site_url("admin/support/create")?>">Add</a></li>
			            <li><a href="<?=site_url("admin/support")?>">Travel Supporter List</a></li>
					</ul>
			    </li>
			</ul>

		</div>
		<div class="sidebar_footer_logo">
			<img src="<?=site_url()?>assets/admin/src/logo_neu_tran.png" alt="">
			<p>Copyright 2018 Neumerlin Co.,Ltd.</p>
		</div>
</div>
<div class="sidebar_mobile">
	<i class="icon-menu"></i>
</div>