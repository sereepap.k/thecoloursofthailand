<style type="text/css">
            .del_guarantee,.del_appeal,.del_consider,.del_borrow,.del_withdraw,.del_withdraw_detail,.del_sold,.del_investigate{
              position: absolute;
              top: 0px;
              right: 0px;
            }
            hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 10px solid rgba(150,0,0,.8);
}
          </style>
          <style type="text/css">
            .vid-name{
              margin-top: 20px;
            }
                .detail-pic,.ui-state-highlight2{
                    position: relative;
                    display: inline-block;
                    margin: 20px;
                }
                .mail-section{
                  position: relative;
                }
                .detail-pic .pic-del,.mail-section .mail-del{
                    position: absolute;
                    right: -5px;
                    top: -5px;
                }
                .pic-holder{
                  overflow-x: auto;
                  max-height: 100px;
                }
                .inp-bg-yellow{
                  background-color:#ffeb0080;
                }
                #main_pic_show{
                	max-width: 400px;
                }
          </style>
<div class="dashboard">
	<?
	$this->load->view('admin/v_dashboard_nav');
	?>
	<div class="dashboard_main">
		<h1>Travel Supporter</h1>
		<div class="dashboard_area scrollbar">
		<!-- dashboard scroll -->
			<form id="f-admin" method="post" action="<?=site_url("admin/support/create")?>" class="dash_form">
				<?
		          if (isset($edit)) {
		            ?>
		            <input type="hidden" name="edit" value="<?=$support->id?>">
		            <?
		          }
		          ?>
				<div class="dash_form_row">
					<div class="cell-l">
						<p>Type :</p>
					</div>
					<div class="cell-r">
						<select id="type" name="type">
						  <option value="transfer">Transfer and luggage services</option>
						  <option value="lifestyles">Experiences and Lifestyles</option>
						  <option value="gastronomy">Gastronomy</option>
						  <option value="wellness">Wellness</option>
						  <option value="planner">Trip Planner</option>
						  <option value="sports">Sports</option>
						</select>
					</div>
				</div>
				<?
		          if (isset($edit)) {
		            ?>
		            <script type="text/javascript">
		              $("#type").val("<?=$support->type?>");
		            </script>
		            <?
		          }
		          ?>
				<div class="dash_form_row">
					<div class="cell-l">
						<p>No. :</p>
					</div>
					<div class="cell-r">
						<input type="text" name="sort_order" id="sort_order" <?if (isset($edit)) { echo "value='".$support->sort_order."'";}?>>
					</div>
				</div>
				<div class="dash_form_row">
					<div class="cell-l">
						<p>Name. :</p>
					</div>
					<div class="cell-r">
						<input type="text" name="name" id="name" <?if (isset($edit)) { echo "value='".$support->name."'";}?>>
					</div>
				</div>
				<div class="dash_form_row">
					<div class="cell-l">
						<p>Body text :</p>
					</div>
					<div class="cell-r">
						<textarea name="body_text" id="body_text" rows="4"><?if (isset($edit)) { echo $support->body_text;}?></textarea>
					</div>
				</div>
				<div class="dash_form_row">
					<div class="cell-l">
						<p>Deal :</p>
					</div>
					<div class="cell-r">
						<textarea name="deal" id="deal" rows="4"><?if (isset($edit)) { echo $support->deal;}?></textarea>
					</div>
				</div>
				<div class="dash_form_row">
					<div class="cell-l">
						<p>Website Link. :</p>
					</div>
					<div class="cell-r">
						<input type="text" name="web_link" id="web_link" <?if (isset($edit)) { echo "value='".$support->web_link."'";}?>>
					</div>
				</div>
				<div class="dash_form_row">
					<div class="cell-l">
						<p>LOGO :</p>
					</div>
					<div class="cell-r">
					</div>
				</div>
				<div class="dash_form_row">
		            <div class="form-group" >
			            <div class="row">
			                <div class="col col-md-12">
			                  <!-- The fileinput-button span is used to style the file input field as button -->
			                  <span class="btn btn-success fileinput-button">
			                      <span>Select files...</span>
			                  <!-- The file input field used as target for the file upload widget -->
			                  <input id="fileupload" type="file" name="files">
			                  </span>
			                  <br>
			                  <br>
			                  <!-- The global progress bar -->
			                  <div id="progress" class="success progress">
			                      <div class="progress-bar bg-success progress-meter" role="progressbar" ></div>
			                  </div>
			                  <!-- The container for the uploaded files -->
			                  <div id="pic_detail_holder" class="cell-r"> 
			                    <?
			                    if (isset($edit)) {
			                      ?>
			                      <div class="detail-pic">
			                            <img id="main_pic_show" src="<?=site_url("media/support/".$support->main_pic)?>">
			                            <input id="main_pic_hid" type="hidden" name="main_pic" value="old_file_picture__<?=$support->main_pic?>">                            
			                        </div>
			                      <?
			                    }else{
			                    ?>
			                        <div class="detail-pic">
			                            <img id="main_pic_show" src="">
			                            <input id="main_pic_hid" type="hidden" name="main_pic" value="">                            
			                        </div>
			                    <?
			                    }
			                    ?>                                
			                  </div>
			                </div>
			            </div>
			          </div>
		          </div>
				<div class="dash_form_row">
					<div class="cell-l">
					</div>
					<div class="cell-r text-center-moblie">
						<button type="button" class="btn btn-light submit">บันทึก</button>
						<button type="reset" value="Reset" class="focus">ยกเลิก</button>
					</div>
				</div>
			</form>

		<!-- dashboard -->
		</div>
	</div>

</div>
<script type="text/javascript">
  $(document).on("click", ".submit", function() {
        $( "#f-admin" ).submit();
        
    });
</script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="<?=site_url()?>/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="<?=site_url()?>/js/jquery.fileupload.js"></script>
<script type="text/javascript">
  $(function() {
	'use strict';
      // Change this to the location of your server-side upload handler:
      var url = '<?php echo site_url('upload/fileupload');?>';
      $('#fileupload').fileupload({
              url: url,
              dataType: 'json',
              done: function(e, data) {
                  //console.log(data);
                  $.each(data.result.files, function(index, file) {
                      //console.log(file);
                      $("#main_pic_show").attr("src","<?php echo site_url('media/temp'); ?>/" + file.name);
                      $("#main_pic_hid").val(file.name);                    
                  });
              },
              progressall: function(e, data) {
                  var progress = parseInt(data.loaded / data.total * 100, 10);
                  $('#progress .progress-meter').css(
                      'width',
                      progress + '%'
                  );
              }
          }).prop('disabled', !$.support.fileInput)
          .parent().addClass($.support.fileInput ? undefined : 'disabled');
});
$(document).on('click', ".pic-del", function(){
                var current_pic=$(this);
                if(confirm('Confirm delete?')) {
                    $("#pic_detail_holder").append('<input type="hidden" name="del_pic[]" value="'+current_pic.attr("att-id")+'">');
                    current_pic.parent().fadeOut(300, function() {
                                            $(this).remove();
                                        });
                }

});
$( "#pic_detail_holder" ).sortable({
      placeholder: "ui-state-highlight2"
    });
    $( "#pic_detail_holder" ).disableSelection();
</script>