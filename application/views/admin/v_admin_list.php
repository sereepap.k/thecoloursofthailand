<?
$ci =& get_instance();
?>
<div class="dashboard">	
	<?
	$this->load->view('admin/v_dashboard_nav');
	?>
	<div class="dashboard_main">
		<h1>Admin List</h1>
		<div class="dashboard_area dashboard_area_element scrollbar scrollbar-table">
		<!-- dashboard scroll -->
			<div class="dash_table">
				<table class="scroll">
				  <thead>
				    <tr>
				      <th>ลำดับ</th>
				      <th>Username</th>
			          <th>สร้าง/แก้ไข Account</th>
			          <th>จัดการข้อมูล</th>
			          <th>ลบข้อมูล</th>
			          <th>Action</th>
				    </tr>
				  </thead>
				  <tbody>
				    <?
			        foreach ($user_list as $key => $value) {
			          ?>
			          <tr>
			            <td><?=$key+1?></td>
			            <td><a href="<?=site_url("admin/admin_user/create/".$value->id)?>"><?=$value->username?></a></td>
			            <td>สร้าง/แก้ไข Account<input type="checkbox" name="user_permission" class="c_perm" u-id="<?=$value->id?>" value="account" <?if(isset($value->perm['account'])){ echo "checked";}?>></td>
			            <td>จัดการข้อมูล<input type="checkbox" name="user_permission" class="c_perm" u-id="<?=$value->id?>" value="data" <?if(isset($value->perm['data'])){ echo "checked";}?>></td>            
			            <td>ลบข้อมูล<input type="checkbox" name="user_permission" class="c_perm" u-id="<?=$value->id?>" value="delete" <?if(isset($value->perm['delete'])){ echo "checked";}?>></td>            
			                      
			            <td>
			            	<a href="<?=site_url("admin/admin_user/create/".$value->id)?>">Edit</a>
			            	<br>
			            	<a href="javascript:deleteconfirm('<?=site_url("admin/admin_user/delete/".$value->id)?>');">Delete</a>
			            </td>
			          </tr>
			          <?
			        }
			        ?>   
				  </tbody>
				</table>
			</div>
		<!-- dashboard -->
		</div>
	</div>
</div>
<script type="text/javascript">
      function deleteconfirm(link){
        if (confirm("Confirm Delete")) {
            window.open(link,"_self")
        };
    }
    $(document).on("change", ".c_perm", function() {
      var c_perm=$( this );
      var state="del";
      if (c_perm.prop( "checked" )){
        state="ins";
      }
        $.ajax({
                        method: "POST",
                        url: "<?php echo site_url("admin/admin_user/ajax_set_perm"); ?>",
                        data: {
                            "id": c_perm.attr("u-id"),
                            "perm": c_perm.val(),
                            "state": state,
                        }
                    })
                    .done(function(data) {
                      if (data['flag']=="1") {
                        c_perm.prop( "checked", true );
                      }else if(data['flag']=="0"){
                        c_perm.prop( "checked", false );
                      }else{
                        alert("some wrong");
                      }
                    });     
        
    });
    </script>