<?
$ci =& get_instance();
?>
<div class="dashboard_nav">
		<div class="dashboard_nav_left">
			<img src="<?=site_url()?>assets/admin/src/logo_client.png" alt="">
		</div>
		<div class="dashboard_nav_right">
			<img src="http://placehold.it/50x50" id="nav_user" alt="">
			<ul id="dashboard_lang">
				<li>TH</li>
				<li style="display: none;">EN</li>
			</ul>
		</div>
		<div class="dashboard_nav_user">
			<div class="dashboard_nav_user-top">
				<h1><?=$ci->user_data->firstname." ".$ci->user_data->lastname?></h1>
				<p>Administrator</p>
				<p class="close_nav_user">X</p>
			</div>
			<div class="dashboard_nav_user-bottom">
				<a class="dashboard_nav_user-btn" href="<?=site_url("admin/Admin_user/create/".$ci->session->userdata('id'))?>">Edit Account</a>
				<a class="dashboard_nav_user-btn btn-half" href="<?=site_url("admin/Admin_user/create")?>">Add Account</a>
				<a class="dashboard_nav_user-btn btn-half" href="<?=site_url("admin/main/logout")?>">Logout</a>
			</div>
		</div>
	</div>