<style type="text/css">
            .del_guarantee,.del_appeal,.del_consider,.del_borrow,.del_withdraw,.del_withdraw_detail,.del_sold,.del_investigate{
              position: absolute;
              top: 0px;
              right: 0px;
            }
            hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 10px solid rgba(150,0,0,.8);
}
          </style>
          <style type="text/css">
            .vid-name{
              margin-top: 20px;
            }
                .detail-pic,.ui-state-highlight2{
                    position: relative;
                    display: inline-block;
                    margin: 20px;
                }
                .mail-section{
                  position: relative;
                }
                .detail-pic .pic-del,.mail-section .mail-del{
                    position: absolute;
                    right: -5px;
                    top: -5px;
                }
                .pic-holder{
                  overflow-x: auto;
                  max-height: 100px;
                }
                .inp-bg-yellow{
                  background-color:#ffeb0080;
                }
                #main_pic_show{
                	max-width: 400px;
                }
          </style>
<div class="dashboard">
	<?
	$this->load->view('admin/v_dashboard_nav');
	?>
	<div class="dashboard_main">
		<h1>Explore</h1>
		<div class="dashboard_area scrollbar">
		<!-- dashboard scroll -->
			<form id="f-admin" method="post" action="<?=site_url("admin/explore/create")?>" class="dash_form">
				<?
		          if (isset($edit)) {
		            ?>
		            <input type="hidden" name="edit" value="<?=$explore->id?>">
		            <?
		          }
		          ?>
				<div class="dash_form_row">
					<div class="cell-l">
						<p>Type :</p>
					</div>
					<div class="cell-r">
						<select id="type" name="type">
						  	<option value="Flavor">Flavor</option>
							<option value="Nature">Nature</option>
							<option value="Splash">Splash</option>
							<option value="Purity">Purity</option>
						</select>
					</div>
				</div>
				<?
		          if (isset($edit)) {
		            ?>
		            <script type="text/javascript">
		              $("#type").val("<?=$explore->type?>");
		            </script>
		            <?
		          }
		          ?>
				<div class="dash_form_row">
					<div class="cell-l">
						<p>No. :</p>
					</div>
					<div class="cell-r">
						<input type="text" name="sort_order" id="sort_order" <?if (isset($edit)) { echo "value='".$explore->sort_order."'";}?>>
					</div>
				</div>
				<div class="dash_form_row">
					<div class="cell-l">
						<p>Location (100 characters) :</p>
					</div>
					<div class="cell-r">
						<textarea name="location" id="location" rows="4"><?if (isset($edit)) { echo $explore->location;}?></textarea>
					</div>
				</div>
				<div class="dash_form_row">
					<div class="cell-l">
						<p>What is it (300 characters) :</p>
					</div>
					<div class="cell-r">
						<textarea name="what_is_it" id="what_is_it" rows="4"><?if (isset($edit)) { echo $explore->what_is_it;}?></textarea>
					</div>
				</div>
				<div class="dash_form_row">
					<div class="cell-l">
						<p>How to get here (300 characters) :</p>
					</div>
					<div class="cell-r">
						<textarea name="how_to" id="how_to" rows="4"><?if (isset($edit)) { echo $explore->how_to;}?></textarea>
					</div>
				</div>
				<div class="dash_form_row">
					<div class="cell-l">
						<p>Thumbnail :</p>
					</div>
					<div class="cell-r">
					</div>
				</div>
				<div class="dash_form_row">
		            <div class="form-group" >
			            <div class="row">
			                <div class="col col-md-12">
			                  <!-- The fileinput-button span is used to style the file input field as button -->
			                  <span class="btn btn-success fileinput-button">
			                      <span>Select files...</span>
			                  <!-- The file input field used as target for the file upload widget -->
			                  <input id="fileupload2" type="file" name="files">
			                  </span>
			                  <br>
			                  <br>
			                  <!-- The global progress bar -->
			                  <div id="progress2" class="success progress">
			                      <div class="progress-bar bg-success progress-meter" role="progressbar" ></div>
			                  </div>
			                  <!-- The container for the uploaded files -->
			                  <div id="pic_detail_holder" class="cell-r"> 
			                    <?
			                    if (isset($edit)) {
			                      ?>
			                      <div class="detail-pic">
			                            <img id="main_pic_show" src="<?=site_url("media/explore_pic/".$explore->main_pic)?>">
			                            <input id="main_pic_hid" type="hidden" name="main_pic" value="old_file_picture__<?=$explore->main_pic?>">                            
			                        </div>
			                      <?
			                    }else{
			                    ?>
			                        <div class="detail-pic">
			                            <img id="main_pic_show" src="">
			                            <input id="main_pic_hid" type="hidden" name="main_pic" value="">                            
			                        </div>
			                    <?
			                    }
			                    ?>                                
			                  </div>
			                </div>
			            </div>
			          </div>
		          </div>
		        <div class="dash_form_row">
					<div class="cell-l">
						<p>Slider :</p>
					</div>
					<div class="cell-r">
					</div>
				</div>
				<div class="dash_form_row">
		            <div class="col col-md-12">
		              <!-- The fileinput-button span is used to style the file input field as button -->
		              <span class="btn btn-success fileinput-button">
		                  <span>Select files...</span>
		              <!-- The file input field used as target for the file upload widget -->
		              <input id="fileupload" type="file" name="files[]" multiple>
		              </span>
		              <br>
		              <br>
		              <!-- The global progress bar -->
		              <div id="progress" class="success progress">
		                  <div class="progress-bar bg-success progress-meter" role="progressbar" ></div>
		              </div>
		              <!-- The container for the uploaded files -->
		              <div id="pic_detail_holder2" class="cell-r"> 
		                <?
		                if (isset($edit)) {
			                foreach ($item as $key => $value) {
			                    ?>
			                    <div class="detail-pic">
			                        <a class="btn btn-success pic-del" att-id="<?=$value->id?>" href="javascript:;">X</a>
			                        <img src="<?php echo site_url('media/explore_pic/'.$value->filepath); ?>" width="100" height="100">
			                        <input type="hidden" name="pic_detail[<?=$value->id?>]" value="old_file_picture__<?=$value->id?>">
			                    </div>
			                    <?
			                }
			            }
		                ?>
		                            
		              </div>
		            </div>
		          </div>
				<div class="dash_form_row">
					<div class="cell-l">
					</div>
					<div class="cell-r text-center-moblie">
						<button type="button" class="btn btn-light submit">บันทึก</button>
						<button type="reset" value="Reset" class="focus">ยกเลิก</button>
					</div>
				</div>
			</form>

		<!-- dashboard -->
		</div>
	</div>

</div>
<script type="text/javascript">
  $(document).on("click", ".submit", function() {
        $( "#f-admin" ).submit();
        
    });
</script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="<?=site_url()?>/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="<?=site_url()?>/js/jquery.fileupload.js"></script>
<script type="text/javascript">
  $(function() {

    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = '<?php echo site_url('upload/fileupload');?>';
    $('#fileupload').fileupload({
            url: url,
            dataType: 'json',
            done: function(e, data) {
                //console.log(data);
                $.each(data.result.files, function(index, file) {
                    //console.log(file);
                    $("#pic_detail_holder2").append('<div class="detail-pic">'+
                                '<a class="btn btn-success pic-del" att-id="no" href="javascript:;">X</a>'+
                                '<img src="<?php echo site_url('media/temp'); ?>/' + file.name+'" width="100" height="100">'+
                                '<input type="hidden" name="pic_detail[]" value="' + file.name+'">'+
                            '</div>')
                });
            },
            progressall: function(e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress .progress-meter').css(
                    'width',
                    progress + '%'
                );
            }
        }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');

        'use strict';
	      // Change this to the location of your server-side upload handler:
	      var url = '<?php echo site_url('upload/fileupload');?>';
	      $('#fileupload2').fileupload({
	              url: url,
	              dataType: 'json',
	              done: function(e, data) {
	                  //console.log(data);
	                  $.each(data.result.files, function(index, file) {
	                      //console.log(file);
	                      $("#main_pic_show").attr("src","<?php echo site_url('media/temp'); ?>/" + file.name);
	                      $("#main_pic_hid").val(file.name);                    
	                  });
	              },
	              progressall: function(e, data) {
	                  var progress = parseInt(data.loaded / data.total * 100, 10);
	                  $('#progress2 .progress-meter').css(
	                      'width',
	                      progress + '%'
	                  );
	              }
	          }).prop('disabled', !$.support.fileInput)
	          .parent().addClass($.support.fileInput ? undefined : 'disabled');
});
$(document).on('click', ".pic-del", function(){
                var current_pic=$(this);
                if(confirm('Confirm delete?')) {
                    $("#pic_detail_holder2").append('<input type="hidden" name="del_pic[]" value="'+current_pic.attr("att-id")+'">');
                    current_pic.parent().fadeOut(300, function() {
                                            $(this).remove();
                                        });
                }

});
$( "#pic_detail_holder2" ).sortable({
      placeholder: "ui-state-highlight2"
    });
    $( "#pic_detail_holder2" ).disableSelection();
</script>