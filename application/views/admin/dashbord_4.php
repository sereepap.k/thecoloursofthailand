<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>NeuMerline/VIRT : BackEnd</title>
	<link rel="stylesheet" href="<?=site_url()?>assets/admin/css.css">
	<link rel="stylesheet" href="<?=site_url()?>assets/admin/fonts/fonts.css">
</head>
<body>
<div class="sidebar">
		<div class="sidebar_search">
			<form>
				<input class="sidebar_search_input" type="text" />
				<button class="sidebar_search_button"><i class="icon-search"></i></button>
			</form>
		</div>
		<div class="sidebar_text">
			<p>Back End :<br>Web Content Management System</p>
		</div>
		<div class="sidebar_collapse scrollbar">
			<ul id="collapse">
			    <li><a class="collapse_main"><i class="icon-paragraph-right"></i>Dashbord</a>
			        <ul><li><a href="">Sub</a></li>
			            <li><a href="">Sub</a></li>
			            <li><a href="">Sub-2</a>
			                <ul><li><a href="">Sub-2</a></li>
			                    <li><a href="">Sub-2</a></li>
			                    <li><a href="">Sub-2</a>
			                        <!-- <ul><li><a href="">Sub-3</a></li></ul> -->
			                    </li>
			                </ul>
			            </li>
			        </ul>
			    </li>
			    <li><a class="collapse_main"><i class="icon-home2"></i>Home</a>
					<ul><li><a href="">Sub</a></li>
			            <li><a href="">Sub</a></li>
 						<li><a href="">Sub-2</a>
			                <ul><li><a href="">Sub-2</a></li>
			                    <li><a href="">Sub-2</a></li>
			                    <li><a href="">Sub-2</a>
			                        <!-- <ul><li><a href="">Sub-3</a></li></ul> -->
			                    </li>
			                </ul>
			            </li>
					</ul>
			    </li>
			    <li><a class="collapse_main"><i class="icon-cog"></i>Other</a>
					<ul><li><a href="">Sub</a></li>
			            <li><a href="">Sub</a></li>
 						<li><a href="">Sub-2</a>
			                <ul><li><a href="">Sub-2</a></li>
			                    <li><a href="">Sub-2</a></li>
			                    <li><a href="">Sub-2</a>
			                        <!-- <ul><li><a href="">Sub-3</a></li></ul> -->
			                    </li>
			                </ul>
			            </li>
					</ul>
			    </li>
			</ul>

		</div>
		<div class="sidebar_footer_logo">
			<img src="<?=site_url()?>assets/admin/src/logo_neu_tran.png" alt="">
			<p>COPYRIGHT 2017 Neumerlin Co.,Ltd</p>
		</div>
</div>
<div class="sidebar_mobile">
	<i class="icon-menu"></i>
</div>
<div class="dashboard">
	<div class="dashboard_nav">
		<div class="dashboard_nav_left">
			<img src="<?=site_url()?>assets/admin/src/logo_client.png" alt="">
		</div>
		<div class="dashboard_nav_right">
			<img src="http://placehold.it/50x50" id="nav_user" alt="">
			<ul id="dashboard_lang">
				<li>TH</li>
				<li style="display: none;">EN</li>
			</ul>
		</div>
		<div class="dashboard_nav_user">
			<div class="dashboard_nav_user-top">
				<h1>test</h1>
				<p>Administrator</p>
				<p class="close_nav_user">X</p>
			</div>
			<div class="dashboard_nav_user-bottom">
				<a class="dashboard_nav_user-btn" href="#">Edit Account</a>
				<a class="dashboard_nav_user-btn btn-half" href="#">Add Account</a>
				<a class="dashboard_nav_user-btn btn-half" href="#">Logout</a>
			</div>
		</div>
	</div>
	<div class="dashboard_main">
		<h1>ค้นหาสถานพยาบาล</h1>
		<div class="dashboard_area scrollbar">
		<!-- dashboard scroll -->
			<form action="" class="dash_form">
				<div class="dash_form_row">
					<div class="cell-l">
						<p>ชื่อโรงพยาบาล / คลินิก :</p>
					</div>
					<div class="cell-r">
						<input type="text">
					</div>
				</div>
				<div class="dash_form_row">
					<div class="cell-l">
						<p>เบอร์โทรศัพท์ :</p>
					</div>
					<div class="cell-r">
						<input type="text">
					</div>
				</div>
				<div class="dash_form_row">
					<div class="cell-l">
						<p>อีเมล :</p>
					</div>
					<div class="cell-r">
						<input type="text">
					</div>
				</div>
				<div class="dash_form_row">
					<div class="cell-l">
						<p>เว็บไซต์ :</p>
					</div>
					<div class="cell-r">
						<input type="text">
					</div>
				</div>
				<div class="dash_form_row">
					<div class="cell-l">
						<p>ที่อยู่ :</p>
					</div>
					<div class="cell-r">
						<textarea rows="2"></textarea>
					</div>
				</div>
				<div class="dash_form_row">
					<div class="cell-l">
						<p>จังหวัด :</p>
					</div>
					<div class="cell-r">
						<select>
						  <option value="BKK">กรุงเทพมหานคร</option>
						  <option value="KBI">กระบี่</option>
						</select>
					</div>
				</div>
				<div class="dash_form_row">
					<div class="cell-l">
						<p>ประเภทบัตร :</p>
					</div>
					<div class="cell-r">
						<select>
						  <option value="Gold">Kpi-Gold-Care</option>
						  <option value="Silver">Kpi-Silver-Care</option>
						</select>
					</div>
				</div>
				<div class="dash_form_row">
					<div class="cell-l">
						<p>ประเภทบัตร :</p>
					</div>
					<div class="cell-r">
					    <input id='radio-1' type="radio" name='r-group-1' value="Hospital" checked='checked' />
    					<label for="radio-1">โรงพยาบาล</label><br>
    					<input id='radio-2' type="radio" name='r-group-1' value="Hos" />
    					<label for="radio-2">คลีนิค</label>
					</div>
				</div>
				<div class="dash_form_row dash_form_text">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque saepe repellat dolorem eveniet ipsum nihil itaque molestias
				</div>
				<div class="dash_form_row">
					<div class="cell-l">
					</div>
					<div class="cell-r text-center-moblie">
					    <input id='check-1' type="checkbox" name='c-1' value="Published" />
    					<label for="check-1">Published</label>
					</div>
				</div>
				<div class="dash_form_row">
					<div class="cell-l">
					</div>
					<div class="cell-r text-center-moblie">
						<button>Save</button>
						<button class="focus">Cancel</button>
					</div>
				</div>
			</form>

		<!-- dashboard -->
		</div>
	</div>

</div>

</body>
</html>
<script src="<?=site_url()?>assets/admin/js/jquery.min.js"></script>
<script src="<?=site_url()?>assets/admin/js/main.js"></script>

