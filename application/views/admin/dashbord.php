<?
$ci =& get_instance();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>NeuMerline/VIRT : BackEnd</title>
	<link rel="stylesheet" href="<?=site_url()?>assets/admin/css.css">
	<link rel="stylesheet" href="<?=site_url()?>assets/admin/fonts/fonts.css">
</head>
<body>
<div class="sidebar">
		<div class="sidebar_search">
			<form>
				<input class="sidebar_search_input" type="text" />
				<button class="sidebar_search_button"><i class="icon-search"></i></button>
			</form>
		</div>
		<div class="sidebar_text">
			<p>Back End :<br>Web Content Management System</p>
		</div>
		<div class="sidebar_collapse scrollbar">
			<ul id="collapse">
			    <li><a class="collapse_main"><i class="icon-paragraph-right"></i>Dashbord</a>
			        <ul><li><a href="">Sub</a></li>
			            <li><a href="">Sub</a></li>
			            <li><a href="">Sub-2</a>
			                <ul><li><a href="">Sub-2</a></li>
			                    <li><a href="">Sub-2</a></li>
			                    <li><a href="">Sub-2</a>
			                        <!-- <ul><li><a href="">Sub-3</a></li></ul> -->
			                    </li>
			                </ul>
			            </li>
			        </ul>
			    </li>
			    <li><a class="collapse_main"><i class="icon-home2"></i>Home</a>
					<ul><li><a href="">Sub</a></li>
			            <li><a href="">Sub</a></li>
 						<li><a href="">Sub-2</a>
			                <ul><li><a href="">Sub-2</a></li>
			                    <li><a href="">Sub-2</a></li>
			                    <li><a href="">Sub-2</a>
			                        <!-- <ul><li><a href="">Sub-3</a></li></ul> -->
			                    </li>
			                </ul>
			            </li>
					</ul>
			    </li>
			    <li><a class="collapse_main"><i class="icon-cog"></i>Other</a>
					<ul><li><a href="">Sub</a></li>
			            <li><a href="">Sub</a></li>
 						<li><a href="">Sub-2</a>
			                <ul><li><a href="">Sub-2</a></li>
			                    <li><a href="">Sub-2</a></li>
			                    <li><a href="">Sub-2</a>
			                        <!-- <ul><li><a href="">Sub-3</a></li></ul> -->
			                    </li>
			                </ul>
			            </li>
					</ul>
			    </li>
			</ul>

		</div>
		<div class="sidebar_footer_logo">
			<img src="<?=site_url()?>assets/admin/src/logo_neu_tran.png" alt="">
			<p>Copyright 2018 Neumerlin Co.,Ltd.</p>
		</div>
</div>
<div class="sidebar_mobile">
	<i class="icon-menu"></i>
</div>
<div class="dashboard">
	<div class="dashboard_nav">
		<div class="dashboard_nav_left">
			<img src="<?=site_url()?>assets/admin/src/logo_client.png" alt="">
		</div>
		<div class="dashboard_nav_right">
			<img src="http://placehold.it/50x50" id="nav_user" alt="">
			<ul id="dashboard_lang">
				<li>TH</li>
				<li style="display: none;">EN</li>
			</ul>
		</div>
		<div class="dashboard_nav_user">
			<div class="dashboard_nav_user-top">
				<h1>test</h1>
				<p>Administrator</p>
				<p class="close_nav_user">X</p>
			</div>
			<div class="dashboard_nav_user-bottom">
				<a class="dashboard_nav_user-btn" href="#">Edit Account</a>
				<a class="dashboard_nav_user-btn btn-half" href="#">Add Account</a>
				<a class="dashboard_nav_user-btn btn-half" href="#">Logout</a>
			</div>
		</div>
	</div>
	<div class="dashboard_main">
		<h1>Dashboard</h1>
		<div class="dashboard_area scrollbar">
		<!-- dashboard -->
			<div class="dash">
				<div class="dash_section gold">
					<div class="dash_box">
						<h1>1990</h1>
						<p>User</p>
					</div>
					<div class="dash_box">
						<h1>300</h1>
						<p>User Login Today</p>
					</div>
				</div>

				<div class="dash_section blue">
					<div class="dash_box">
						<h1>599</h1>
						<p>แจ้งเคลมออนไลน์<br>ผ่านเว็บไซต์</p>
					</div>
					<div class="dash_box">
						<h1>2000</h1>
						<p>สนใจซื้อประกัน /<br>สอบถามข้อมูล</p>
					</div>
					<div class="dash_box">
						<h1>50</h1>
						<p>แนะนํา /  <br>ร้องเรียนบริการ</p>
					</div>
					<div class="dash_box">
						<h1>400</h1>
						<p>สมัครงาน</p>
					</div>
					<div class="dash_box">
						<h1>599</h1>
						<p>สมัคร ตัวแทน / นายหน้า</p>
					</div>
				</div>
			</div>
		<!-- dashboard -->
		</div>
	</div>

</div>

</body>
</html>
<script src="<?=site_url()?>assets/admin/js/jquery.min.js"></script>
<script src="<?=site_url()?>assets/admin/js/main.js"></script>

