<style type="text/css">
            .del_guarantee,.del_appeal,.del_consider,.del_borrow,.del_withdraw,.del_withdraw_detail,.del_sold,.del_investigate{
              position: absolute;
              top: 0px;
              right: 0px;
            }
            hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 10px solid rgba(150,0,0,.8);
}
          </style>
          <style type="text/css">
            .vid-name{
              margin-top: 20px;
            }
                .detail-pic,.ui-state-highlight2{
                    position: relative;
                    display: inline-block;
                    margin: 20px;
                }
                .mail-section{
                  position: relative;
                }
                .detail-pic .pic-del,.mail-section .mail-del{
                    position: absolute;
                    right: -5px;
                    top: -5px;
                }
                .pic-holder{
                  overflow-x: auto;
                  max-height: 100px;
                }
                .inp-bg-yellow{
                  background-color:#ffeb0080;
                }
                #main_pic_show{
                	max-width: 400px;
                }
          </style>
          
<div class="dashboard">
	<?
	$this->load->view('admin/v_dashboard_nav');
	?>
	<div class="dashboard_main">
		<h1>Home</h1>
		<div class="dashboard_area scrollbar">
		<!-- dashboard scroll -->
			<form id="f-admin" method="post" action="<?=site_url("admin/home")?>" class="dash_form">
				<?
		          if (isset($edit)) {
		            ?>
		            <input type="hidden" name="edit" value="<?=$home->id?>">
		            <?
		          }
		          ?>
				<div class="dash_form_row">
					<div class="cell-l">
						<p>Youtube Video 1:</p>
					</div>
					<div class="cell-r">
						<input type="text" name="ytvid1" id="ytvid1" <?if (isset($edit)) { echo "value='".$home->ytvid1."'";}?>>
					</div>
				</div>
				<div class="dash_form_row">
					<div class="cell-l">
						<p>Example</p>
					</div>
					<div class="cell-r">
						<label>https://www.youtube.com/embed/EG_Oai3r5wA</label>
					</div>
				</div>
				<div class="dash_form_row">
					<div class="cell-l">
					</div>
					<div class="cell-r">
						<iframe width="560" height="315" src="<?=$home->ytvid1?>" id="video1"  allowscriptaccess="always" allow="autoplay"></iframe>
					</div>
				</div>
				<div class="dash_form_row">
					<div class="cell-l">
						<p>Youtube Video 2:</p>
					</div>
					<div class="cell-r">
						<input type="text" name="ytvid2" id="ytvid2" <?if (isset($edit)) { echo "value='".$home->ytvid2."'";}?>>
					</div>
				</div>
				<div class="dash_form_row">
					<div class="cell-l">
					</div>
					<div class="cell-r">
						<iframe width="560" height="315" src="<?=$home->ytvid2?>" id="video2"  allowscriptaccess="always" allow="autoplay"></iframe>
					</div>
				</div>				
				<div class="dash_form_row">
					<div class="cell-l">
						<p>BG video 1 :</p>
					</div>
					<div class="cell-r">
					</div>
				</div>
				<div class="dash_form_row">
		            <div class="form-group" >
			            <div class="row">
			                <div class="col col-md-12">
			                  <!-- The fileinput-button span is used to style the file input field as button -->
			                  <span class="btn btn-success fileinput-button">
			                      <span>Select files...</span>
			                  <!-- The file input field used as target for the file upload widget -->
			                  <input id="fileupload2" type="file" name="files">
			                  </span>
			                  <br>
			                  <br>
			                  <!-- The global progress bar -->
			                  <div id="progress2" class="success progress">
			                      <div class="progress-bar bg-success progress-meter" role="progressbar" ></div>
			                      <label id="fin2"></label>
			                  </div>
			                  <!-- The container for the uploaded files -->
			                  <div id="pic_detail_holder" class="cell-r"> 
			                    <?
			                    if (isset($edit)) {
			                      ?>
			                      <div class="detail-pic">
			                      	<video id="vid2" autoplay muted loop>
										<source id="main_pic_show2" src="<?=site_url("media/vid/".$home->bgvid1)?>" type="video/mp4">
									</video>
			                            <input id="main_pic_hid2" type="hidden" name="bgvid1" value="old_file_picture__<?=$home->bgvid1?>">                            
			                        </div>
			                      <?
			                    }else{
			                    ?>
			                        <div class="detail-pic">
			                            <video autoplay muted loop>
											<source id="main_pic_show2" src="" type="video/mp4">
										</video>
			                            <input id="main_pic_hid2" type="hidden" name="bgvid1" value="">                            
			                        </div>
			                    <?
			                    }
			                    ?>                          
			                  </div>
			                </div>
			            </div>
			          </div>
		          </div>
		          <div class="dash_form_row">
					<div class="cell-l">
						<p>BG video 2 :</p>
					</div>
					<div class="cell-r">
					</div>
				</div>
				<div class="dash_form_row">
		            <div class="form-group" >
			            <div class="row">
			                <div class="col col-md-12">
			                  <!-- The fileinput-button span is used to style the file input field as button -->
			                  <span class="btn btn-success fileinput-button">
			                      <span>Select files...</span>
			                  <!-- The file input field used as target for the file upload widget -->
			                  <input id="fileupload" type="file" name="files">
			                  </span>
			                  <br>
			                  <br>
			                  <!-- The global progress bar -->
			                  <div id="progress" class="success progress">
			                      <div class="progress-bar bg-success progress-meter" role="progressbar" ></div>
			                      <label id="fin1"></label>
			                  </div>
			                  <!-- The container for the uploaded files -->
			                  <div id="pic_detail_holder" class="cell-r"> 
			                    <?
			                    if (isset($edit)) {
			                      ?>
			                      <div class="detail-pic">
			                      	<video id="vid1" autoplay muted loop>
										<source id="main_pic_show" src="<?=site_url("media/vid/".$home->bgvid2)?>" type="video/mp4">
									</video>
			                            <input id="main_pic_hid" type="hidden" name="bgvid2" value="old_file_picture__<?=$home->bgvid2?>">                            
			                        </div>
			                      <?
			                    }else{
			                    ?>
			                        <div class="detail-pic">
			                            <video autoplay muted loop>
											<source id="main_pic_show2" src="" type="video/mp4">
										</video>
			                            <input id="main_pic_hid2" type="hidden" name="bgvid2" value="">                            
			                        </div>
			                    <?
			                    }
			                    ?>                          
			                  </div>
			                </div>
			            </div>
			          </div>
		          </div>
				<div class="dash_form_row">
					<div class="cell-l">
					</div>
					<div class="cell-r text-center-moblie">
						<button type="button" class="btn btn-light submit">บันทึก</button>
						<button type="reset" value="Reset" class="focus">ยกเลิก</button>
					</div>
				</div>
			</form>
		<!-- dashboard -->
		</div>
	</div>

</div>
<script type="text/javascript">
  $(document).on("click", ".submit", function() {
        $( "#f-admin" ).submit();
        
    });
</script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="<?=site_url()?>/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="<?=site_url()?>/js/jquery.fileupload.js"></script>
<script type="text/javascript">
  $(function() {

  	'use strict';
	      // Change this to the location of your server-side upload handler:
	      var url = '<?php echo site_url('upload/fileupload');?>';
	      $('#fileupload').fileupload({
	              url: url,
	              dataType: 'json',
	              done: function(e, data) {
	                  //console.log(data);
	                  $.each(data.result.files, function(index, file) {
	                      //console.log(file);
	                      $("#main_pic_show").attr("src","<?php echo site_url('media/temp'); ?>/" + file.name);
	                      $("#main_pic_hid").val(file.name);
	                      var video = document.getElementById('vid1');
	                      video.load();
	                      video.play();
	                      $("#fin1").html("finish!!!");                    
	                  });
	              },
	              progressall: function(e, data) {
	              	$("#fin1").html("");
	                  var progress = parseInt(data.loaded / data.total * 100, 10);
	                  $('#progress .progress-meter').css(
	                      'width',
	                      progress + '%'
	                  );
	              }
	          }).prop('disabled', !$.support.fileInput)
	          .parent().addClass($.support.fileInput ? undefined : 'disabled');

        'use strict';
	      // Change this to the location of your server-side upload handler:
	      var url = '<?php echo site_url('upload/fileupload');?>';
	      $('#fileupload2').fileupload({
	              url: url,
	              dataType: 'json',
	              done: function(e, data) {
	                  //console.log(data);
	                  $.each(data.result.files, function(index, file) {
	                      //console.log(file);
	                      $("#main_pic_show2").attr("src","<?php echo site_url('media/temp'); ?>/" + file.name);
	                      $("#main_pic_hid2").val(file.name);    
	                      var video = document.getElementById('vid2');
	                      video.load();
	                      video.play();
	                      $("#fin2").html("finish!!!");                
	                  });
	              },
	              progressall: function(e, data) {
	              	$("#fin2").html("");
	                  var progress = parseInt(data.loaded / data.total * 100, 10);
	                  $('#progress2 .progress-meter').css(
	                      'width',
	                      progress + '%'
	                  );
	              }
	          }).prop('disabled', !$.support.fileInput)
	          .parent().addClass($.support.fileInput ? undefined : 'disabled');
});
$( "#pic_detail_holder2" ).sortable({
      placeholder: "ui-state-highlight2"
    });
    $( "#pic_detail_holder2" ).disableSelection();

$('#ytvid1').on('change', function (e) {
		$("#video1").attr('src',$(this).val() + "?autoplay=1&amp;modestbranding=1&amp;showinfo=0" ); 
	})   
$('#ytvid2').on('change', function (e) {
		$("#video2").attr('src',$(this).val() + "?autoplay=1&amp;modestbranding=1&amp;showinfo=0" ); 
	}) 	 
</script>