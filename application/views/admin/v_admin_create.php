<div class="dashboard">
	<?
	$this->load->view('admin/v_dashboard_nav');
	?>
	<div class="dashboard_main">
		<h1>Create admin</h1>
		<div class="dashboard_area scrollbar">
		<!-- dashboard scroll -->
			<form id="f-admin" method="post" action="<?=site_url("admin/admin_user/create")?>" class="dash_form">
				<?
		          if (isset($edit)) {
		            ?>
		            <input type="hidden" name="edit" value="<?=$user->id?>">
		            <?
		          }
		          ?>
				<div class="dash_form_row">
					<div class="cell-l">
						<p>คำนำหน้าชื่อ :</p>
					</div>
					<div class="cell-r">
						<select id="prefix" name="prefix">
						  	<option>--- เลือกคำนำหน้าชื่อ ---</option>
			                <option>นาย</option>
			                <option>นาง</option>
			                <option>นางสาว</option>
			                <option>เด็กหญิง</option>
			                <option>เด็กชาย</option>
						</select>
					</div>
				</div>
				<?
		          if (isset($edit)) {
		            ?>
		            <script type="text/javascript">
		              $("#prefix").val("<?=$user->prefix?>");
		            </script>
		            <?
		          }
		          ?>
				<div class="dash_form_row">
					<div class="cell-l">
						<p>ขื่อ :</p>
					</div>
					<div class="cell-r">
						<input type="text" name="name" id="name" <?if (isset($edit)) { echo "value='".$user->firstname."'";}?>>
					</div>
				</div>
				<div class="dash_form_row">
					<div class="cell-l">
						<p>นามสกุล :</p>
					</div>
					<div class="cell-r">
						<input type="text" name="surname" id="surname" <?if (isset($edit)) { echo "value='".$user->lastname."'";}?>>
					</div>
				</div>
				<div class="dash_form_row">
					<div class="cell-l">
						<p>อีเมล :</p>
					</div>
					<div class="cell-r">
						<input type="text" name="email" id="email" <?if (isset($edit)) { echo "value='".$user->email."'";}?>>
					</div>
				</div>
				<div class="dash_form_row">
					<div class="cell-l">
						<p>Username :</p>
					</div>
					<div class="cell-r">
						<input type="text" name="username" id="username" <?if (isset($edit)) { echo "value='".$user->username."' disabled ";}?>>
					</div>
				</div>
				<div class="dash_form_row">
					<div class="cell-l">
						<p>Password :</p>
					</div>
					<div class="cell-r">
						<input type="password" name="password" id="password" <?if (isset($edit)) { echo "value='".$user->password."'";}?>>
					</div>
				</div>
				<div class="dash_form_row">
					<div class="cell-l">
						<p>Confirm Password :</p>
					</div>
					<div class="cell-r">
						<input type="password" name="con_password" id="con_password" <?if (isset($edit)) { echo "value='".$user->password."'";}?>>
					</div>
				</div>
				<div class="dash_form_row">
					<div class="cell-l">
					</div>
					<div class="cell-r text-center-moblie">
						<button type="button" class="btn btn-light submit">บันทึก</button>
						<button type="reset" value="Reset" class="focus">ยกเลิก</button>
					</div>
				</div>
			</form>

		<!-- dashboard -->
		</div>
	</div>

</div>
<script type="text/javascript">
  $(document).on("click", ".submit", function() {
        $( "#f-admin" ).submit();
        
    });
</script>