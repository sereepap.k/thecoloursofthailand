<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
        parent::__construct();  
        $this->load->model('m_explore');
        $this->load->model('m_support');
        $this->load->model('m_home');   
    }
	public function index()
	{
		$data["page"]="home";
		$data["flavour"]=$this->m_explore->get_all_explore(0,0,"sort_order","asc",array('type' => "Flavor"));
		$data["nature"]=$this->m_explore->get_all_explore(0,0,"sort_order","asc",array('type' => "Nature"));
		$data["splash"]=$this->m_explore->get_all_explore(0,0,"sort_order","asc",array('type' => "Splash"));
		$data["purity"]=$this->m_explore->get_all_explore(0,0,"sort_order","asc",array('type' => "Purity"));

		$data["transfer"]=$this->m_support->get_all_support(0,0,"sort_order","asc",array('type' => "transfer"));
		$data["lifestyles"]=$this->m_support->get_all_support(0,0,"sort_order","asc",array('type' => "lifestyles"));
		$data["gastronomy"]=$this->m_support->get_all_support(0,0,"sort_order","asc",array('type' => "gastronomy"));
		$data["wellness"]=$this->m_support->get_all_support(0,0,"sort_order","asc",array('type' => "wellness"));
		$data["planner"]=$this->m_support->get_all_support(0,0,"sort_order","asc",array('type' => "planner"));
		$data["sports"]=$this->m_support->get_all_support(0,0,"sort_order","asc",array('type' => "sports"));

		$data["home"]=$this->m_home->get_contact();

		$this->load->view('home',$data);
	}

	public function render_modal()
	{
		$explore=$this->m_explore->get_explore_by_id($_POST["id"]);
		$item=$this->m_explore->get_pic_by_explore_id($explore->id);
		?>
		<div class="row">
                        <div class="col-12 col-lg-6">
                            <div class="owl-carousel">
                            	<?
                            	foreach ($item as $key => $value) {
                            		?>
                            		<div class="slides"><img src="<?php echo site_url('media/explore_pic/'.$value->filepath); ?>" alt="" /></div>
                            		<?
                            	}
                            	?>
                            </div>
                            <div id="counter"></div>
                        </div>
                        <div class="col-12 col-lg-6">
                            <div class="content2 pl-lg-2">
                                <p class="text-small">Location</p>
                                <h5 class="modal-title"><?=$explore->location?></h5>
                                <h6>WHAT IS IT?</h6>
                                <p><?=$explore->what_is_it?></p>
                                <h6>HOW TO GET HERE</h6>
                                <p><?=$explore->how_to?></p>
                            </div>
                        </div>
                    </div>
		<?
	}
}
