<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct() {
        parent::__construct();      
        $this->load->model('m_admin');  
        $this->load->model('m_home');  
        if ($this->session->userdata('id')) {
            $user_data = $this->m_admin->get_admin_by_id($this->session->userdata('id'));
            if (isset($user_data->username)&&isset($user_data->perm['data'])) {
                $this->user_data = $user_data;
            }
            else {
                redirect('admin/main/logout');
            }
        }
        else {
            redirect('admin/main/logout');
        }
    }

	private function create_ins_array($id,$post,$edit=false,$obj=null)
  {
  	print_r($post);
    $data_ins = array(
        'id' => $id,
        'ytvid1' => $post['ytvid1'],
        'ytvid2' => $post['ytvid2'],
      );

    if (isset($post['bgvid1'])) {
                $pic_val=$post['bgvid1'];
                    $pos = strpos($pic_val, "old_file_picture__");
                    if ($pos === false) {
                        if ($edit) {
                            unlink("./media/vid/".$obj->bgvid1);
                        }
                        //echo "in here 1 ";
                        $item_id=$id."-".time();                        
                        $ext=explode(".", $pic_val);
                        $new_ext=$ext[count($ext)-1];
                        $new_filename=$item_id."_bgvid1.".$new_ext;
                        $file = './media/temp/'.$pic_val;
                        $newfile = "./media/vid/".$new_filename;
                        if (!is_dir("./media/vid/")) {
                                mkdir("./media/vid/");
                            }                        
                        if (!copy($file, $newfile)) {
                            echo "failed to copy $file...\n".$file." to ".$newfile;
                            @unlink("./media/temp/".$pic_val);
                            @unlink("./media/temp/thumbnail/".$pic_val);
                        }else{   
                            $data_ins['bgvid1']=$new_filename;
                            @unlink("./media/temp/".$pic_val);
                            @unlink("./media/temp/thumbnail/".$pic_val);                            
                        }
                      }            
        }
        if (isset($post['bgvid2'])) {
                $pic_val=$post['bgvid2'];
                    $pos = strpos($pic_val, "old_file_picture__");
                    if ($pos === false) {
                        if ($edit) {
                            unlink("./media/vid/".$obj->bgvid2);
                            echo $obj->bgvid2;
                        }
                        //echo "in here 1 ";
                        $item_id=$id."-".time();                        
                        $ext=explode(".", $pic_val);
                        $new_ext=$ext[count($ext)-1];
                        $new_filename=$item_id."_bgvid2.".$new_ext;
                        $file = './media/temp/'.$pic_val;
                        $newfile = "./media/vid/".$new_filename;
                        if (!is_dir("./media/vid/")) {
                                mkdir("./media/vid/");
                            }                        
                        if (!copy($file, $newfile)) {
                            echo "failed to copy $file...\n".$file." to ".$newfile;
                            @unlink("./media/temp/".$pic_val);
                            @unlink("./media/temp/thumbnail/".$pic_val);
                        }else{   
                            $data_ins['bgvid2']=$new_filename;
                            @unlink("./media/temp/".$pic_val);
                            @unlink("./media/temp/thumbnail/".$pic_val);                            
                        }
                      }            
        }
    return $data_ins;

  }
	public function index()
	{
        $home=$this->m_home->get_contact();
		if (isset($_POST['ytvid1'])&&isset($_POST['edit'])) {
				$id=$_POST['edit'];
				$data_up=$this->create_ins_array($id,$_POST,true,$home);
					$this->m_home->update_contact($data_up,$id);
					?>
				        <script type="text/javascript">
				        	alert("บันทึกข้อมูลเรียบร้อยแล้ว");
				            window.open("<?echo site_url('admin/home');?>","_self");            
				        </script>
				    <?
		}
		$id=$this->uri->segment(4,'');	
        $home=$this->m_home->get_contact();	
		$data['page'] = 'home';
		if (isset($home->ytvid1)) {
			$data['home']=$home;
			$data['edit']=$home->id;
		}
		$this->load->view('admin/v_head',$data);
        $this->load->view('admin/v_sidebar',$data);
        $this->load->view('admin/v_home',$data);
        $this->load->view('admin/v_footer',$data);
	}

	
}
