<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Explore extends CI_Controller {

	public function __construct() {
        parent::__construct();      
        $this->load->model('m_admin');  
        $this->load->model('m_explore');  
        if ($this->session->userdata('id')) {
            $user_data = $this->m_admin->get_admin_by_id($this->session->userdata('id'));
            if (isset($user_data->username)&&isset($user_data->perm['data'])) {
                $this->user_data = $user_data;
            }
            else {
                redirect('admin/main/logout');
            }
        }
        else {
            redirect('admin/main/logout');
        }
    }
	public function index()
	{
		$filter = array();
		if (isset($_POST["type"])) {
			$filter["type"]=$_POST["type"];
		}
		$data['page'] = 'user';
		$data['explore_list'] = $this->m_explore->get_all_explore(0,0,"sort_order","asc",$filter);
		$this->load->view('admin/v_head',$data);
		$this->load->view('admin/v_sidebar',$data);
		$this->load->view('admin/v_explore_list',$data);
		$this->load->view('admin/v_footer',$data);
	}

	private function create_ins_array($id,$post,$edit=false,$obj=null)
  {
  	print_r($post);
    $data_ins = array(
        'id' => $id,
        'type' => $post['type'],
        'sort_order' => $post['sort_order'],
        'location' => $post['location'],
        'what_is_it' => $post['what_is_it'],
        'how_to' => $post['how_to'],        
        
      );    
    if (!$edit) {
    	$data_ins["datecreate"]=time();
    }
    if (isset($post['main_pic'])) {
                $pic_val=$post['main_pic'];
                    $pos = strpos($pic_val, "old_file_picture__");
                    if ($pos === false) {
                        if ($edit) {
                            @unlink("./media/explore_pic/".$obj->main_pic);
                        }
                        //echo "in here 1 ";
                        $item_id=$id."-".time();                        
                        $ext=explode(".", $pic_val);
                        $new_ext=$ext[count($ext)-1];
                        $new_filename=$item_id."_main.".$new_ext;
                        $file = './media/temp/'.$pic_val;
                        $newfile = "./media/explore_pic/".$new_filename;
                        if (!is_dir("./media/explore_pic/")) {
                                mkdir("./media/explore_pic/");
                            }                        
                        if (!copy($file, $newfile)) {
                            echo "failed to copy $file...\n".$file." to ".$newfile;
                            @unlink("./media/temp/".$pic_val);
                            @unlink("./media/temp/thumbnail/".$pic_val);
                        }else{   
                            $data_ins['main_pic']=$new_filename;
                            @unlink("./media/temp/".$pic_val);
                            @unlink("./media/temp/thumbnail/".$pic_val);                            
                        }
                      }            
        }
    if (isset($post['del_pic'])) {
            foreach ($post['del_pic'] as $key => $value) {
                $this->m_explore->del_pic($value);
            }			
		}
		if (isset($post['pic_detail'])) {
            $sort_order=0;
            foreach ($post['pic_detail'] as $key => $value) {
                $sort_order+=1;
                    $pos = strpos($value, "old_file_picture__");
                    if ($pos === false) {
                        //echo "in here 1 ";
                        $item_id=$this->m_explore->gen_pic_id();                        
                        $ext=explode(".", $value);
                        $new_ext=$ext[count($ext)-1];
                        $new_filename=$item_id.".".$new_ext;
                        $file = './media/temp/'.$value;
                        $newfile = "./media/explore_pic/".$new_filename;
                        if (!is_dir("./media/explore_pic/")) {
                                mkdir("./media/explore_pic/");
                            }
                        $item_data = array(
                        'id' => $item_id, 
                        'explore_id' => $id, 
                        'sort_order' => $sort_order, 
                        'filepath' => $new_filename 
                        );
                        if (!copy($file, $newfile)) {
                            echo "failed to copy $file...\n".$file." to ".$newfile;
                            @unlink("./media/temp/".$value);
                            @unlink("./media/temp/thumbnail/".$value);
                        }else{
                        	$this->m_explore->add_pic($item_data);   
                            @unlink("./media/temp/".$value);
                            @unlink("./media/temp/thumbnail/".$value);                            
                        }
                      }else{
                        $pic_id=explode("__", $value);
                        //echo "in here";
                        $item_id=$pic_id[1];
                        $item_data = array(
                            'sort_order' => $sort_order, 
                        );
                        $this->m_explore->update_pic($item_data,$item_id);
                        
                      }                
                
            }
        }
    return $data_ins;

  }
	public function delete()
	{
		$id=$this->uri->segment(4,'');
		if (isset($this->user_data->perm['delete'])) {
			$this->m_explore->delete_explore($id);
			?>
				        <script type="text/javascript">
				        	alert("ลบข้อมูลเรียบร้อยแล้ว");
				            window.open("<?echo site_url('admin/explore');?>","_self");            
				        </script>
				    <?
		}else{
			?>
				        <script type="text/javascript">
				        	alert("ไม่มีสิทธิลบข้อมูล");
				            window.open("<?echo site_url('admin/explore');?>","_self");            
				        </script>
				    <?
		}
	}
	public function create()
	{
		if (isset($_POST['location'])&&!isset($_POST['edit'])) {
				$id=$this->m_explore->generate_id();
				$data_up=$this->create_ins_array($id,$_POST);
					$this->m_explore->add_explore($data_up);
					?>
				        <script type="text/javascript">
				        	alert("บันทึกข้อมูลเรียบร้อยแล้ว");
				            window.open("<?echo site_url('admin/explore');?>","_self");            
				        </script>
				    <?

		}
		if (isset($_POST['location'])&&isset($_POST['edit'])) {
				$id=$_POST['edit'];
				$explore=$this->m_explore->get_explore_by_id($id);
				$data_up=$this->create_ins_array($id,$_POST,true,$explore);
					$this->m_explore->update_explore($data_up,$id);
					?>
				        <script type="text/javascript">
				        	alert("บันทึกข้อมูลเรียบร้อยแล้ว");
				            window.open("<?echo site_url('admin/explore');?>","_self");            
				        </script>
				    <?
		}
		$id=$this->uri->segment(4,'');
		$explore=$this->m_explore->get_explore_by_id($id);
		$data['page'] = 'explore';
		if (isset($explore->location)) {
			$data['explore']=$explore;
			$data['item']=$this->m_explore->get_pic_by_explore_id($explore->id);
			$data['edit']=$explore->id;
		}
		$this->load->view('admin/v_head',$data);
		$this->load->view('admin/v_sidebar',$data);
		$this->load->view('admin/v_explore_create',$data);
		$this->load->view('admin/v_footer',$data);
	}
}
