<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	public function __construct() {
        parent::__construct();   
        $this->load->model('m_admin');
        $this->load->model('m_time');
        if (!$this->session->has_userdata('stu_keyword')) {
             	$this->session->set_userdata('stu_keyword', "");
        }
        if (!$this->session->has_userdata('start_date')) {
             	$this->session->set_userdata('start_date', "");
        }
        if (!$this->session->has_userdata('end_date')) {
             	$this->session->set_userdata('end_date', "");
        }
        if (!$this->session->has_userdata('limit')) {
             	$this->session->set_userdata('limit', 12);
        }     
    }
	public function index()
	{
		$data['page'] = 'home';
		$this->load->view('admin/v_head',$data);
		$this->load->view('admin/v_login',$data);
		$this->load->view('admin/v_footer',$data);
		
	}
	public function dashboard()
	{
		if ($this->session->userdata('id')) {
            $user_data = $this->m_admin->get_admin_by_id($this->session->userdata('id'));
            if (isset($user_data->username)) {
                $this->user_data = $user_data;
            }
            else {
                redirect('admin/main/logout');
            }
        }
        else {
            redirect('admin/main/logout');
        }
		$data['page'] = 'home';
		$data['month_name_arr'] = $this->m_stringlib->month_name_arr;
		$data['day_name_arr'] = $this->m_stringlib->day_name_arr;
		$this->load->view('admin/v_head',$data);
		$this->load->view('admin/v_sidebar',$data);
		$this->load->view('admin/v_dashbord',$data);
		$this->load->view('admin/v_footer',$data);

	}
	public function recover_pass()
	{
		$user_dat=$this->m_admin->get_admin_by_email($_POST['email']);
		if (isset($user_dat->username)) {
			require_once('./PHPMailer/PHPMailerAutoload.php');
		        //Create a new PHPMailer instance
		        $mail = new PHPMailer;

		        //Tell PHPMailer to use SMTP
		        $mail->isSMTP();
		        $mail->SMTPOptions = array(
		            'ssl' => array(
		                'verify_peer' => false,
		                'verify_peer_name' => false,
		                'allow_self_signed' => true
		            )
		        );
		        //Enable SMTP debugging
		        // 0 = off (for production use)
		        // 1 = client messages
		        // 2 = client and server messages
		        $mail->SMTPDebug = 0;

		        //Ask for HTML-friendly debug output
		        $mail->Debugoutput = 'html';

		        //Set the hostname of the mail server
		        $mail->Host = 'smtp.gmail.com';
		        // use
		        // $mail->Host = gethostbyname('smtp.gmail.com');
		        // if your network does not support SMTP over IPv6

		        //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
		        $mail->Port = 587;

		        //Set the encryption system to use - ssl (deprecated) or tls
		        $mail->SMTPSecure = 'tls';

		        //Whether to use SMTP authentication
		        $mail->SMTPAuth = true;
		        $mail->CharSet = 'UTF-8';
		        //Username to use for SMTP authentication - use full email address for gmail
		        $mail->Username = "sereepap.k@neumerlin.com";

		        //Password to use for SMTP authentication
		        $mail->Password = "gliu4kr8elu";

		        //Set who the message is to be sent from
		        $mail->setFrom('sereepap.k@neumerlin.com', 'Salad Web Contact');

		        //Set an alternative reply-to address
		        $mail->addReplyTo('sereepap.k@neumerlin.com', 'Salad Web Contact');

		        //Set who the message is to be sent to
		        //$mail->addAddress('tumavait.v@neumerlin.com', 'Tumavait Vijakkhana');
		        $mail->addAddress($user_dat->email, $user_dat->username);

		        //Set the subject line
		        $mail->Subject = 'Recover password';

		        //Read an HTML message body from an external file, convert referenced images to embedded,
		        $mail->isHTML(true);
		        $mail->Body = 'Username, <b>'.$user_dat->username.'</b><br>Password, <b>'.$user_dat->password.'</b>';


		        //send the message, check for errors        
		      ?>
		      <script type="text/javascript">
		      <?
		      if (!$mail->send()) {
		            ?>
		            alert("Failed to send E-mail");
		            console.log("<?="Mailer Error: " . $mail->ErrorInfo?>")
		            window.open("<?php echo site_url('admin');?>","_self"); 
		            <?
		        } else {
		            ?>
		                alert('ส่งข้อมูลไป E-mail ที่ระบุไว้แล้ว');
		                window.open("<?php echo site_url('admin');?>","_self");      
		            <?
		        }
		      ?>
		        
		      </script>
		      <?
		}else{
		        $_SESSION['pass_wrong']="yes"
			?>
		        <script type="text/javascript">
		                alert('ไม่มี E-mail นี้ในระบบ');
		        		window.open("<?php echo site_url('admin');?>","_self");	        	
		        </script>
				<?php 
		}

	}
	public function dashboard1()
	{
		$this->load->view('admin/dashbord');

	}
	public function dashboard2()
	{
		$this->load->view('admin/dashbord_2');

	}
	public function dashboard3()
	{
		$this->load->view('admin/dashbord_3');

	}
	public function dashboard4()
	{
		$this->load->view('admin/dashbord_4');

	}
	private function quick_sort($array)
	{
		// find array size
		$length = count($array);
		
		// base case test, if array of length 0 then just return array to caller
		if($length <= 1){
			return $array;
		}
		else{
		
			// select an item to act as our pivot point, since list is unsorted first position is easiest
			$pivot = $array[0];
			
			// declare our two arrays to act as partitions
			$left = $right = array();
			
			// loop and compare each item in the array to the pivot value, place item in appropriate partition
			for($i = 1; $i < count($array); $i++)
			{
				if($array[$i]->quick_sort_data < $pivot->quick_sort_data){
					$left[] = $array[$i];
				}
				else{
					$right[] = $array[$i];
				}
			}
			
			// use recursion to now sort the left and right lists
			return array_merge($this->quick_sort($left), array($pivot), $this->quick_sort($right));
		}
	}
	public function login()
	{	
	$data['error_msg2']='Please login with your username and password';
	$this->load->view('admin/v_head',$data);
	if(isset($_POST['username']))
		{
			$user_data=$this->m_admin->get_by_username_password($_POST['username'],$_POST['password']);
			//echo $_POST['login_name']." asdasd ".$_POST['password'];
			if (isset($user_data->username)) {
				$this->session->set_userdata('username', $user_data->username);
				$this->session->set_userdata('id', $user_data->id);
				redirect('admin/home');

			}else{			
				
				$this->load->view('admin/v_login',$data);
				
				$this->session->sess_destroy();
			}			
		}else{
			$this->load->view('login',$data);
			$this->session->sess_destroy();
		}	
		$this->load->view('admin/v_footer',$data);
	}
	public function logout()
	{		
		$this->session->set_userdata('username', '');
		$this->session->set_userdata('id', '');
		$this->session->sess_destroy();
		redirect('admin/main');
	}
}
