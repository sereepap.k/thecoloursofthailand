<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Support extends CI_Controller {

	public function __construct() {
        parent::__construct();      
        $this->load->model('m_admin');  
        $this->load->model('m_support');  
        if ($this->session->userdata('id')) {
            $user_data = $this->m_admin->get_admin_by_id($this->session->userdata('id'));
            if (isset($user_data->username)&&isset($user_data->perm['data'])) {
                $this->user_data = $user_data;
            }
            else {
                redirect('admin/main/logout');
            }
        }
        else {
            redirect('admin/main/logout');
        }
    }
	public function index()
	{
		$filter = array();
		if (isset($_POST["type"])) {
			$filter["type"]=$_POST["type"];
		}
		$data['page'] = 'user';
		$data['support_list'] = $this->m_support->get_all_support(0,0,"sort_order","asc",$filter);
		$this->load->view('admin/v_head',$data);
		$this->load->view('admin/v_sidebar',$data);
		$this->load->view('admin/v_support_list',$data);
		$this->load->view('admin/v_footer',$data);
	}

	private function create_ins_array($id,$post,$edit=false,$obj=null)
  {
  	print_r($post);
    $data_ins = array(
        'id' => $id,
        'type' => $post['type'],
        'sort_order' => $post['sort_order'],
        'body_text' => $post['body_text'],
        'deal' => $post['deal'],
        'web_link' => $post['web_link'],
        'name' => $post['name'],
      );    
    if (!$edit) {
    	$data_ins["datecreate"]=time();
    }
    if (isset($post['main_pic'])) {
                $pic_val=$post['main_pic'];
                    $pos = strpos($pic_val, "old_file_picture__");
                    if ($pos === false) {
                        if ($edit) {
                            @unlink("./media/support/".$obj->main_pic);
                        }
                        //echo "in here 1 ";
                        $item_id=$id."-".time();                        
                        $ext=explode(".", $pic_val);
                        $new_ext=$ext[count($ext)-1];
                        $new_filename=$item_id."_main.".$new_ext;
                        $file = './media/temp/'.$pic_val;
                        $newfile = "./media/support/".$new_filename;
                        if (!is_dir("./media/support/")) {
                                mkdir("./media/support/");
                            }                        
                        if (!copy($file, $newfile)) {
                            echo "failed to copy $file...\n".$file." to ".$newfile;
                            @unlink("./media/temp/".$pic_val);
                            @unlink("./media/temp/thumbnail/".$pic_val);
                        }else{   
                            $data_ins['main_pic']=$new_filename;
                            @unlink("./media/temp/".$pic_val);
                            @unlink("./media/temp/thumbnail/".$pic_val);                            
                        }
                      }            
        }
    return $data_ins;

  }
	public function delete()
	{
		$id=$this->uri->segment(4,'');
		if (isset($this->user_data->perm['delete'])) {
			$this->m_support->delete_support($id);
			?>
				        <script type="text/javascript">
				        	alert("ลบข้อมูลเรียบร้อยแล้ว");
				            window.open("<?echo site_url('admin/support');?>","_self");            
				        </script>
				    <?
		}else{
			?>
				        <script type="text/javascript">
				        	alert("ไม่มีสิทธิลบข้อมูล");
				            window.open("<?echo site_url('admin/support');?>","_self");            
				        </script>
				    <?
		}
	}
	public function create()
	{
		if (isset($_POST['name'])&&!isset($_POST['edit'])) {
				$id=$this->m_support->generate_id();
				$data_up=$this->create_ins_array($id,$_POST);
					$this->m_support->add_support($data_up);
					?>
				        <script type="text/javascript">
				        	alert("บันทึกข้อมูลเรียบร้อยแล้ว");
				            window.open("<?echo site_url('admin/support');?>","_self");            
				        </script>
				    <?

		}
		if (isset($_POST['name'])&&isset($_POST['edit'])) {
				$id=$_POST['edit'];
				$support=$this->m_support->get_support_by_id($id);
				$data_up=$this->create_ins_array($id,$_POST,true,$support);
					$this->m_support->update_support($data_up,$id);
					?>
				        <script type="text/javascript">
				        	alert("บันทึกข้อมูลเรียบร้อยแล้ว");
				            window.open("<?echo site_url('admin/support');?>","_self");            
				        </script>
				    <?
		}
		$id=$this->uri->segment(4,'');
		$support=$this->m_support->get_support_by_id($id);
		$data['page'] = 'support';
		if (isset($support->name)) {
			$data['support']=$support;
			$data['edit']=$support->id;
		}
		$this->load->view('admin/v_head',$data);
		$this->load->view('admin/v_sidebar',$data);
		$this->load->view('admin/v_support_create',$data);
		$this->load->view('admin/v_footer',$data);
	}
}
