<?php
class M_home extends CI_Model
{
    
    public function __construct() {
        parent::__construct();
        $this->load->model("m_stringlib");
    }    

    function update_contact($data, $id) {
        $this->db->where('id', $id);
        $this->db->update('home', $data);
    }
    function get_contact() {
        $business = new stdClass();
        $query = $this->db->get_where('home');
        
        if ($query->num_rows() > 0) {
            $business = $query->result();
            $business = $business[0];
        }
        return $business;
    }
}
