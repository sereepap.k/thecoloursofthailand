<?php
class M_explore extends CI_Model
{
    
    public function __construct() {
        parent::__construct();
        $this->load->model("m_stringlib");
    }
    
    function generate_id() {
        $isuniq = FALSE;
        $clam_id = '';
        do {
            $temp_id = $this->m_stringlib->uniqueAlphaNum10();
            $query = $this->db->get_where('explore', array('id' => $temp_id));
            if ($query->num_rows() == 0) {
                $clam_id = $temp_id;
                $isuniq = TRUE;
            }
        } while (!$isuniq);
        
        return $clam_id;
    }
    function gen_pic_id() {
        $isuniq = FALSE;
        $clam_id = '';
        do {
            $temp_id = $this->m_stringlib->uniqueAlphaNum10();
            $query = $this->db->get_where('explore_pic', array('id' => $temp_id));
            if ($query->num_rows() == 0) {
                $clam_id = $temp_id;
                $isuniq = TRUE;
            }
        } while (!$isuniq);
        
        return $clam_id;
    }

    function add_explore($data) {
        $this->db->insert('explore', $data);
    }
    function update_explore($data, $id) {
        $this->db->where('id', $id);
        $this->db->update('explore', $data);
    }
    function delete_explore($id) { 
        $explore=$this->get_explore_by_id($id);
        $explore_pic=$this->get_pic_by_explore_id($id);
        foreach ($explore_pic as $key => $value) {
            $this->del_pic($value->id);
        }
        @unlink("./media/explore_pic/".$explore->main_pic);
        $this->db->where('id', $id);
        $this->db->delete('explore');
    }
    function count_all() {
        $query = $this->db->query("SELECT COUNT(*) as r FROM explore;");
        return $query->result()[0]->r;
    }
    function get_all_explore($offset=0,$limit=0,$order_by="id",$type="asc",$filter = array()) {
        $g_list = array();
        $this->db->order_by($order_by, $type);
        if ($limit!=0) {
            $this->db->limit($limit, $offset);
        }
        foreach ($filter as $key => $value) {
            $this->db->like($key, $value);
        }
        $query = $this->db->get('explore');
        
        if ($query->num_rows() > 0) {
            $g_list = $query->result();
        }
        return $g_list;
    }

    function get_explore_by_id($id) {
        $business = new stdClass();
        $query = $this->db->get_where('explore', array('id' => $id));
        
        if ($query->num_rows() > 0) {
            $business = $query->result();
            $business = $business[0];
        }
        return $business;
    }



    function add_pic($data) {
        $this->db->insert('explore_pic', $data);
    }
    function update_pic($data, $id) {
        $this->db->where('id', $id);
        $this->db->update('explore_pic', $data);
    }
    function del_pic($id) { 
        $gallery=$this->get_pic_by_id($id);
        @unlink("./media/explore_pic/".$gallery->filepath);
        $this->db->where('id', $id);
        $this->db->delete('explore_pic');
    }
    function get_pic_by_explore_id($explore_id,$order_by="sort_order",$type="asc",$filter = array()) {
        $g_list = array();
        $this->db->order_by($order_by, $type);
        foreach ($filter as $key => $value) {
            $this->db->where($key, $value);
        }
        $this->db->where("explore_id", $explore_id);
        $query = $this->db->get('explore_pic');
        
        if ($query->num_rows() > 0) {
            $g_list = $query->result();
        }
        return $g_list;
    }

    function get_pic_by_id($id) {
        $business = new stdClass();
        $query = $this->db->get_where('explore_pic', array('id' => $id));
        
        if ($query->num_rows() > 0) {
            $business = $query->result();
            $business = $business[0];
        }
        return $business;
    }
}
