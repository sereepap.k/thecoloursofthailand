
	$("#login-forgot").hide();

	$("#login-main-btn").click(function(){
		$("#login-main").fadeOut(100);
		$("#login-forgot").delay(200).fadeIn(200);
	});

	$("#login-forgot-btn").click(function(){
		$("#login-forgot").fadeOut(100);
		$("#login-main").delay(200).fadeIn(200);
	});

/*	$("#login-main-submit").click(function(){
		$(".login_box_catch").removeClass("none");
	});*/

	$(".sidebar_mobile").click(function(){
		$(".sidebar_mobile").toggleClass("is-open");
		$(".sidebar").toggleClass("is-open");
		$(".sidebar_footer_logo").toggleClass("is-open");
		$(".dashboard").toggleClass("is-open");
	});

	$("#dashboard_lang").click(function(){
		$("#dashboard_lang li").toggle();
	});

	$("#nav_user,.close_nav_user").click(function(){
		$(".dashboard_nav_user").toggle();
	});

	$("#table-option").click(function(){
		$(".dash_element").toggle();
	});


	$(document).ready(function() {

	    var plus = $('<span>▾</span>').addClass('plus');

	    $('#collapse li').has('ul').addClass('has-child')
	    plus.prependTo('.has-child');

	    $(".has-child").hover(
	        function () {
	            $(this).addClass('hover');
	        },
	        function () {
	            $(this).removeClass('hover');
	    });

	    $('.has-child ul').hide();

	    $('.has-child .plus').click(function(){
	        $('ul:first', this.parentNode).slideToggle();
			/*$(".has-child span").toggle();*/
	    });

	});

