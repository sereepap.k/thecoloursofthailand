
$(function($) {
  
	'use strict'
	$('[data-toggle="offcanvas"]').on('click', function() {
		$('body').toggleClass('open');
		$('.offcanvas-collapse').toggleClass('open');
		$('.navbar-toggler').toggleClass('toggled');
	});

	$(window).on("load",function(){
		$(".content").mCustomScrollbar();
	});
	
	/*
	$(window).load(function(){
				
		$(".content-x").mCustomScrollbar({
			axis:"x",
			theme:"light-3",
			advanced:{autoExpandHorizontalScroll:true}
		});
		
	}); 
	*/

});

